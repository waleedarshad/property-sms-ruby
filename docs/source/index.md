---
title: Property SMS API Reference

language_tabs:
  - shell: cURL

includes:
  - errors

search: true
---


# Introduction

Welcome to the Property SMS API! You can use our API to access Property SMS endpoints, which can get information on various contacts, profile and properties in our database.

<aside class="notice">
  For the sake of example, the hostname is set as localhost:3000. Please change the hostname according to your request.
</aside>

# Agent

## Sign up

```shell
curl -i -X POST \
   -d "agent[email]=umair@applabs.com.au&agent[password]=password&agent[password_confirmation]=password&agent[first_name]=Umair&agent[last_name]=Ejaz&agent[company]=applabs&agent[mobile_number]=123456789&agent[customer_id]=123456789"
 http://app.propertysms.com.au/agents.json
```

> The above command returns JSON structured like this:

```json
{
"id": 1,
"email": "umairejaz.ch+1@gmail.com",
"first_name": null,
"last_name": null,
"company": null,
"mobile_number": null,
"authentication_token": "6bc73eb52e32c1c8822fb9eb",
"created_at": "2015-03-08T19:14:57.275Z",
"message": "Confirmation instructions have been sent to your email. Please check your junk folder if it is not in your inbox."
}  
```

Signing up agent.
### HTTP Request

`POST http://app.propertysms.com.au/agents.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
agent[first_name] |  | The first name of the agent
agent[last_name] |  | The last name of the agent
agent[email] |  | Email of agent
agent[password] |  | Password of the agent
agent[password_confirmation] |  | Password confirmation of the agent
agent[company] |  | The company name of the agent
agent[mobile_number] |  | The mobile_number of the agent
agent[customer_id] |  | That stripe will send 


## Sign in

```shell
curl -i -X POST \
   -d "agent[email]=agent@iwm.com.au&agent[password]=password&agent[udId_android]=a7b01b72f25f9008f9b7066e&agent[device]=android&agent[ud_ID]=a7b01b72f25f9008f9b7066e" http://app.propertysms.com.au/agents/sign_in.json
```

> The above command returns JSON structured like this:

```json
  {"id":12,
  "email":"agent@iwm.com.au",
  "first_name":"agent",
  "last_name":"agent",
  "company":"example",
  "mobile_number":"123456789",
  "authentication_token":"a7b01b72f25f9008f9b7066e",
  "created_at":"2015-04-15T12:56:47.000Z",
  "contacts_count": 4,
  "terms_accepted": true,
  "subscription": "40-80 Contacts"
  "subscription_expired_at": "2015-07-23",
  "subscription_expired": false
  }
```

Signing in a agent.
### HTTP Request

`POST http://app.propertysms.com.au/agents/sign_in.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
agent[email] |  | The email or password of the agent
agent[password] |  | Valid password for user.
agent[udId_android]| | For android devices(device token)
agent[device] | | In Case if Android device is used
agent[ud_ID] || For IOS devices (device token)

## Subscription

```shell
curl -i -X POST \
  -d "authentication_token=6bc73eb52e32c1c8822fb9eb&agent[subscription]=40-80 Contacts http://app.propertysms.com.au/api/agents/subscription.json
```

> The above command returns JSON structured like this:

```json
{
"email": "umairejaz.ch+1@gmail.com",
"subscription": "40-80 Contacts",
"subscription_expired_at": "2015-07-23",
"contacts": 30,
"subscription_expired": false
}  
```
subscription 
### HTTP Request

`POST http://app.propertysms.com.au/agents/subscription.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
agent[subscription] |  | Subscription pakage

## Profile

```shell
curl -i -X POST \
   -d "authentication_token=6bc73eb52e32c1c8822fb9eb&agent[first_name]=Umair&agent[last_name]=Ejaz&agent[company]=applabs&agent[mobile_number]=123456789&agent[customer_id]=123456789
 http://app.propertysms.com.au/api/agents/profile.json
```

> The above command returns JSON structured like this:

```json
{
"id": 1,
"email": "umairejaz.ch+1@gmail.com",
"first_name": "Umair",
"last_name": "Ejaz",
"company": "applabs",
"mobile_number": 123456789,
"authentication_token": "6bc73eb52e32c1c8822fb9eb",
"created_at": "2015-03-08T19:14:57.275Z",
"terms_accpeted": true,
"subscription_expired_at": "2015-07-23",
"message": "Agent has successfully been updated."
}  
```
> For accepting terms and conditions just pass:

```shell
curl -i -X POST -d "authentication_token=6bc73eb52e32c1c8822fb9eb&agent[terms_accepted]=true"
 http://app.propertysms.com.au/api/agents/profile.json
```

Signing up agent.
### HTTP Request

`POST http://app.propertysms.com.au/api/agents/profile.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
agent[first_name] |  | The first name of the agent
agent[last_name] |  | The last name of the agent
agent[company] |  | The company name of the agent
agent[mobile_number] |  | The mobile_number of the agent
agent[customer_id] |  | That stripe will send 



## Sign Out

```shell
curl -i -X DELETE -d "device=android" \
http://app.propertysms.com.au/agents/sign_out.json
```

> The above command returns JSON structured like this:

```json
{
  "success": true
}

```

Signingout a agent

### HTTP Request

`DELETE http://app.propertysms.com.au/agents/sign_out.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
device |  | In case if device is android


# Passwords

## Reset Password

```shell
curl -i -X POST \
-d "agent[email]=agent@iwm.com.au"
http://app.propertysms.com.au/agents/password.json
```

> The above command returns JSON structured like this:

```json
{"message": "Reset password instruction has been sent to your email"}

```

Reset agent's password

### HTTP Request

`POST http://app.propertysms.com.au/agents/password.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
agent[email] |  | The email address to reset password for






# Contacts
## Get Contacts

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/contacts.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/contacts.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id": 1,
    "first_name": "umair",
    "last_name": "ejaz",
    "mobile_number": "3222",
    "partner_first_name": "james",
    "partner_last_name": "bond",
    "partner_mobile_number": "12345678",
    "last_message_sent": "hello hru ...",
    "month_messages": 2,
    "user_id": 3,
    "properties":[
      {
        "id":4,
        "unit_number":"11111",
        "house_number":"22222",
        "street":"3333",
        "investment_property":false,
        "suburb":"test",
        "state":"vic",
        "postcode":"54000",
        "sold": true,
        "created_at":"2015-03-25T12:56:26.000Z"
      }
    ]
  },
  {
    "id": 2,
    "first_name": "Usman",
    "last_name": "butt",
    "mobile_number": "3222",
    "partner_first_name": "james",
    "partner_last_name": "bond",
    "partner_mobile_number": "12345678",
    "last_message_sent": "hello hru ...",
    "month_messages": 0,
    "user_id": 3,
    "properties":[
      {
        "id":5,
        "unit_number":"11111",
        "house_number":"22222",
        "street":"3333",
        "investment_property":false,
        "suburb":"test",
        "state":"vic",
        "postcode":"54000",
        "sold": true,
        "created_at":"2015-03-25T12:56:26.000Z"
      },
      {
        "id":7,
        "unit_number":"11111",
        "house_number":"22222",
        "street":"3333",
        "investment_property":true,
        "suburb":"test",
        "state":"vic",
        "postcode":"54000",
        "sold": false,
        "created_at":"2015-03-25T12:56:26.000Z"
      },
      {
        "id":40,
        "unit_number":"11111",
        "house_number":"22222",
        "street":"3333",
        "investment_property":true,
        "suburb":"test",
        "state":"vic",
        "postcode":"54000",
        "sold": false,
        "created_at":"2015-03-25T12:56:26.000Z"
      },
      {
        "id":64,
        "unit_number":"11111",
        "house_number":"22222",
        "street":"3333",
        "investment_property":true,
        "suburb":"test",
        "state":"vic",
        "postcode":"54000",
        "sold": true,
        "created_at":"2015-03-25T12:56:26.000Z"
      }
    ]
  },
  {
    "id": 3,
    "first_name": "Hasham",
    "last_name": "Malik",
    "mobile_number": "12345678",
    "partner_first_name": "james",
    "partner_last_name": "bond",
    "partner_mobile_number": "12345678",
    "last_message_sent": "",
    "month_messages": 0,
    "user_id": 3,
    "properties":[
    ]
  }
]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.

## Create Contact

```shell
curl -i -X POST \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&contact[first_name]=umair&contact[last_name]=ejaz&contact[mobile_number]=12345678&contact[partner_first_name]=james&contact[partner_last_name]=bond&contact[partner_mobile_number]=12345678"
http://app.propertysms.com.au/api/contacts.json
```
> The above command returns JSON structured like this:

```
{
"id": 93,
"first_name": "first",
"last_name": "last",
"mobile_number": "12345678",
"partner_first_name": null,
"partner_last_name": null,
"partner_mobile_number": null,
"created_at": "2015-07-31T04:54:25.469Z",
"message": "Contact successfully created"
}
  "OR"

{
"id": 2626,
"message": "You have already deleted account with this Mobile Number, Do you want to recreate it?"
}
```
> To Restore Contact,  Call Update Contacts api
> by passing contact ID from Create Contact api response

## Import Contacts

```shell
curl -i -X POST \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&contact[][first_name]=umair&contact[][last_name]=ejaz&contact[][mobile_number]=12345678&contact[][properties][][unit_number]=2&contact[][properties][][house_number]=2&contact[][properties][][street]=23 george brick&contact[][properties][][suburb]=Berri&contact[][properties][][state]=SA&contact[][properties][][postcode]=3244&contact[][properties][][unit_number]=22&contact[][properties][][house_number]=23&contact[][properties][][street]=lamda st&contact[][properties][][suburb]=abc&contact[][properties][][state]=SA&contact[][properties][][postcode]=32232
" http://app.propertysms.com.au/api/contacts.json
```
> The above command returns JSON structured like this:

```json
{
"message": "2 contacts successfully created"
}
```

### HTTP Request

`POST http://app.propertysms.com.au/api/contacts.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
contact[][first_name] |  | First name of contact.
contact[][last_name]  |  | Last name of contact.
contact[][mobile_number] |  | Mobile number of contact.
contact[][properties][][unit_number]  |  | Unit number of contact property.
contact[][properties][][house_number] |  | House number of contact property.
contact[][properties][][street] |  | Street of contact property.
contact[][properties][][suburb] |  | Suburb of contact property.
contact[][properties][][state] |  | State of contact property.
contact[][properties][][postcode] |  | Post code of contact property.
contact[][properties][][unit_number] |  | Unit number of contact property.
contact[][properties][][house_number] |  | House number of contact property.
contact[][properties][][street] |  | Street of contact property.
contact[][properties][][suburb] |  | Suburb of contact property.
contact[][properties][][state] |  | State of  contact property.
contact[][properties][][postcode] |  | Post Code of contact property.

## Add Contacts by email

```shell
curl -i -X POST \
-d "authentication_token=fa9ddbccd52b130407355e23" http://app.propertysms.com.au/api/contacts/add_contacts_by_email.json

```
> The above command returns JSON structured like this:

```json
{
"message": "Import Contact Instruction is sent you your email address, please follow instruction to proceed."
}
```

### HTTP Request

`POST http://app.propertysms.com.au/api/contacts/add_contacts_by_email.json`

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.


## Update Contact

```shell
curl -i -X PATCH \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&contact[first_name]=umair&contact[last_name]=ejaz&contact[mobile_number]=12345678&contact[partner_first_name]=james&contact[partner_last_name]=bond&contact[partner_mobile_number]=12345678"
http://app.propertysms.com.au/api/contacts/:contact_id.json
```
> The above command returns JSON structured like this:

```json
{
"id": 2,
"first_name": "umair",
"last_name": "ejaz",
"mobile_number": "12345678",
"partner_first_name": "james",
"partner_last_name": "bond",
"partner_mobile_number": "12345678",
"created_atß": "2015-03-08T20:03:38.728Z",
"message": "Contact successfully created"
}
```

### HTTP Request

`PATCH http://app.propertysms.com.au/api/contacts/:contact_id.json`
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API </aside>


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
contact_id | | Contact ID
contact[first_name] |  | First name of contact.
contact[last_name] |  | Last name of contact.
contact[mobile_number] |  | Mobile number of contact.
contact[partner_first_name] |  | First name of Partner's contact.
contact[partner_last_name] |  | Last name of Partner's contact.
contact[partner_mobile_number] |  | Mobile number of contact.



## Delete Contact

```shell
curl -i -X DELETE \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb" http://app.propertysms.com.au/api/contacts/:contact_id.json
```
> The above command returns JSON structured like this:

```json
{
"message": "Contact successfully deleted"
}
```

### HTTP Request

`DELETE http://app.propertysms.com.au/api/contacts/:contact_id.json`
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API </aside>


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
contact_id | | Contact ID 




## Get Properties

```shell
curl -i -X POST \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/contacts/:contact_id/properties.json
```
> The above command returns JSON structured like this:

```json
{
"id": 2,
"unit_number": 11111,
"house_number": 1234,
"street": "abcdef",
"investment_property": true,
"suburb": "test",
"state": "vic",
"postcode": 54000,
"sold": true,
"created_atß": "2015-03-08T20:03:38.728Z",
"message": "Property successfully created"
}
```

### HTTP Request

`POST http://app.propertysms.com.au/api/contacts/:contact_id/properties.json `
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API </aside>

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.



## Create Property

```shell
curl -i -X POST \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&property[unit_number]=11111&property[house_number]=1234&property[street]=abcdef&property[investment_property]=true&property[suburb]=test&property[state]=vic&property[postcode]=54000&property[sold]=true"
http://app.propertysms.com.au/api/contacts/:contact_id/properties.json
```
> The above command returns JSON structured like this:

```json
{
"id": 2,
"unit_number": 11111,
"house_number": 1234,
"street": "abcdef",
"investment_property": true,
"suburb": "test",
"state": "vic",
"postcode": 54000,
"sold": true,
"created_atß": "2015-03-08T20:03:38.728Z",
"message": "Property successfully created"
}
```

### HTTP Request

`POST http://app.propertysms.com.au/api/contacts/:contact_id/properties.json `
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API </aside>

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
property[unit_number] |  | Unit number of property.
property[house_number] |  | House number of property.
property[street] |  | Street name of property.
property[investment_property] |  |   its boolean value can be true/false .
property[suburb] |  | Suburb name of property.
property[state] |  | State name of property.
property[postcode] |  | Post Code of property.
property[sold] | | its boolean value can be true/false .



## Update Property

```shell
curl -i -X PATCH \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&property[unit_number]=30&property[house_number]=8899&property[street]=32&property[investment_property]=true&property[suburb]=kabana Iland&property[state]=0&property[postcode]=3200&property[sold]=true"
http://app.propertysms.com.au/api/contacts/:contact_id/properties/:id.json
```
> The above command returns JSON structured like this:

```json
{
  "id":9,
  "unit_number":"30",
  "house_number":"88",
  "street":"32",
  "investment_property":false,
  "suburb":"kabana Iland",
  "state":"0",
  "postcode":"3200",
  "sold":true,
  "created_at":"2015-04-07T13:26:08.000Z",
  "message":"Property successfully Updated"}
```

### HTTP Request

`PATCH http://app.propertysms.com.au/api/contacts/:contact_id/properties/:id.json`
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API and <:id> is the property ID to update</aside>


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
contact_id | | Contact ID.
id | | ID of the property to update
property[unit_number] |  | Unit Number of property.
property[house_number] |  | House Number of property.
property[street] |  | Street of property .
property[state] |  | State of property.
property[postcode] |  | property postcode .
property[sold] |  | sold status it would be true/false.

## Delete Property

```shell
curl -i -X DELETE \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb" http://app.propertysms.com.au/api/contacts/:contact_id/properties/:id.json
```
> The above command returns JSON structured like this:

```json
{
"message": "Property successfully deleted"
}
```

### HTTP Request

`DELETE http://app.propertysms.com.au/api/contacts/:contact_id/properties/:id.json`
<aside class="notice">Where <:contact_id> is the Contact Id that you will get from get-contacts API or create contact API and <:id> is the property ID to delete</aside>

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
id | | Property Id
contact_id | | Contact ID of property



# Intros
## Get Intros

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/intros.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/intros.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id": 1,
    "message": "Hi[ CONTACT NAME ][ USER NAME ][ COMPANY NAME ]",
    "user_id": 3
  },
  {
    "id": 2,
    "message": "INTRO37*. Hi [ CONTACT NAME ], [ USER NAME ] here from [ COMPANY NAME ]. Hope you've been well.",
    "user_id": 3
  }
]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.

## Select Intros

```shell
curl -i -X PUT \
-d "authentication_token=ae4cb524174b1415190c39eb&admin=true&intro_id=6" http://app.propertysms.com.au/api/intros/select_intros.json
```
### HTTP Request

`PUT http://app.propertysms.com.au/api/intros/select_intro.json`

> The above command returns JSON structured like this:

```json
  {
    "success": true
  }
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
admin     |       |  string value to select or unselect(true/false)
intro_id  |         | Intro id to select 


# Outros
## Get Outros

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/outros.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/outros.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id": 1,
    "message": "All the best. [ USER NAME ]. OUTRO21*",
    "user_id": 3
  },
  {
    "id": 2,
    "message": "I'm here to assist if you need anything. [ USER NAME ] from [ COMPANY NAME ]. OUTRO20.",
    "user_id": 3
  }
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.

## Select Outros

```shell
curl -i -X PUT \
-d "authentication_token=ae4cb524174b1415190c39eb&admin=true&outro_id=6" http://app.propertysms.com.au/api/outros/select_outros.json
```
### HTTP Request

`PUT http://app.propertysms.com.au/api/outros/select_outros.json`

> The above command returns JSON structured like this:

```json
  {
    "success": true
  }
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
admin     |       |  String value as true/false
outro_id  |          | Outro id to select 


# Templates
## Get Templates

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/templates.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/templates.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id": 1,
    "group": "E",
    "message": "E10. A property near your investment was recently sold. I thought I would let you know that [ LISTING ADDRESS ] is a [ BxB ] and was sold for [ $XXXXXX ]."
  },
  {
    "group": "A",
    "message": "A2. [ LISTING ADDRESS ], which is only about [ DISTANCE ] from your [ STREET IF INVESTMENT ] property, was recently sold. It is a [ BxB ] and [ $XXXXXX ] was the sale price."
  }
]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.


# Sold Properties
## Get sold Properties

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&latitude=56.8916&longitude=25.4164"
http://app.propertysms.com.au/api/sold_properties.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/sold_properties.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id":201,
    "address_id":"9929078",
    "postcode":"5041",
    "state":"0",
    "property_type":"House",
    "contract_date":null,
    "transaction_date":null,
    "areasize":"667",
    "bedrooms":4,
    "baths":2,
    "parking":2,
    "eventtype_code":"AUSP",
    "event_price":"818000.0",
    "event_date":"2015-03-21",
    "suburb":"Daw Park",
    "street_type":"Av",
    "street_name":"Ayers",
    "street_number":"22",
    "flat_number":"",
    "distance":"okm"
    },
    {"id":202,
    "address_id":"20714265",
    "postcode":"5023",
    "state":"0",
    "property_type":"Unit",
    "contract_date":null,
    "transaction_date":null,
    "areasize":"249",
    "bedrooms":3,
    "baths":2,
    "parking":1,
    "eventtype_code":"PTSD",
    "event_price":"361000.0",
    "event_date":"2015-03-21",
    "suburb":"Findon",
    "street_type":"Av",
    "street_name":"Burt",
    "street_number":"1",
    "flat_number":"5",
    "distance":"4.56km"
    }
  ]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
latitude | | Latitude of logged-in person.
longitude | | Longitude of logged-in person.



## Contacts within range

```shell
curl -i -X GET -d "authentication_token=ae4cb524174b1415190c39eb&latitude=56.8916&longitude=25.4164&range=3200" http://app.propertysms.com.au/api/sold_properties/contacts.json

```
### HTTP Request

`GET http://app.propertysms.com.au/api/sold_properties/contacts.json`

> The above command returns JSON structured like this:

```json
[
  {
    "id":5,
    "first_name":"dd",
    "last_name":"ddd",
    "mobile_number":"33232"
    "properties":[
      {
        "id":9,
        "postcode":"5041",
        "state":"0",
        "suburb":"Daw Park",
        "unit_number":"323",
        "house_number":"80-3",
        "street":"3-A",
        "investment_property":false,
        "sold":true,
        "sold_property_id":201,
        "distance":"4.56km"
      },
      {
        "id":15,
        "postcode":"5041",
        "state":"0",
        "suburb":"Daw Park",
        "unit_number":"11111",
        "house_number":"1234",
        "street":"abcdef",
        "investment_property":false,
        "sold":true,
        "sold_property_id":202,
        "distance":"2.00km"
      }
    ]
  },
  {
    "id":9,
    "first_name":"asif",
    "last_name":"Wasif",
    "mobile_number":"12345678"
    "properties":[
      { 
        "id":22,
        "postcode":"5041",
        "state":"0",
        "suburb":"Daw Park",
        "unit_number":"1233331111",
        "house_number":"12332323",
        "street":"90-A",
        "investment_property":true,
        "sold":true,
        "sold_property_id":205,
        "distance":"1.56km"
      }
    ]
  }
]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
latitude | | Latitude of logged-in person.
longitude | | Longitude of logged-in person.
range | | Range of Suburb.




# Messages
## Get Messages

```shell
curl -i -X GET \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb"
http://app.propertysms.com.au/api/messages.json
```
### HTTP Request

`GET http://app.propertysms.com.au/api/messages.json`

> The above command returns JSON structured like this:

```json
[
  {
    "message_id": 41,
    "contact": {
      "id": 92,
      "first_name": "Salt",
      "last_name": "Malt",
      "mobile_number": "3244009045",
      "partner_first_name": "James",
      "partner_last_name": "Bond",
      "partner_mobile_number": null,
      "user_id": 1,
      "created_at": "2015-07-30T07:22:57.000Z",
      "updated_at": "2015-07-30T07:22:57.000Z",
      "deleted": false
    },
    "property": {
      "id": 429,
      "unit_number": "1",
      "house_number": "3",
      "street": "Muscat Avenue",
      "investment_property": true,
      "suburb": "Berri",
      "state": "SA",
      "postcode": "5343",
      "sold": false,
      "created_at": "2015-07-30T07:22:59.000Z",
      "verified": true
    },
    "sold_property": {
      "id": 229,
      "address_id": "1233213l",
      "postcode": "5343",
      "latitude": "-34.280775",
      "longitude": "140.594243",
      "state": "SA",
      "property_type": "House",
      "contract_date": null,
      "transaction_date": null,
      "areasize": "667",
      "bedrooms": 4,
      "baths": 2,
      "parking": 2,
      "eventtype_code": "AUSP",
      "event_price": "32.0",
      "event_date": "2015-03-21",
      "suburb": "Berri",
      "street_type": null,
      "street_name": "Muscat Ave",
      "street_number": "4",
      "flat_number": "12",
      "created_at": "2015-07-30T09:20:27.000Z",
      "updated_at": "2015-07-30T09:20:27.000Z"
    },
    "distance": 72,
    "last_message_sent_at": "",
    "month_messages": 0,
    "last_message_created_at": "2015-07-30",
    "template": "E",
    "subscription_expired": false
  }
]
```

### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.



## Update Message

```shell
curl -i -X PATCH \
-d "authentication_token=6bc73eb52e32c1c8822fb9eb&message[message_body]=Hi Fernandus, hru ..."
http://app.propertysms.com.au/api/messages/:message_id.json
```
> The above command returns JSON structured like this:

```json
[
  {
    "id": 41,
    "contact_id": 10,
    "sold_property_id": 1,
    "message_body": "Hi Fernandus, hru ...",
    "sent": "true",
    "sent_date_at": "2015-05-11",
    "expired": "false",
    "created_at": "2015-05-11T10:37:00.000Z",
    "updated_at": "2015-05-11T11:50:46.154Z",
    "message": "Message successfully updated"
  }
]
```

### HTTP Request

`PATCH http://app.propertysms.com.au/api/messages/:message_id.json`
<aside class="notice">Where <:message_id> is the Message Id that you will get from get-messages API</aside>


### Query Parameters

Parameter | Default | Description
--------- | ------- | -----------
authenticaion_token | | Authentication token of Agent.
message_body | | Message body that have been sent to contact



