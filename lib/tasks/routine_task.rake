namespace :routine_task do

  desc "Delete message at eleventh day"
  task :delete_message => :environment do
    SoldProperty.messages_expire
  end

  desc "Delete message Notficiation"
  task :delete_message_notification => :environment do
  	puts "--------------- running task -------------------"
  	User.delete_message_notification
  end

end
