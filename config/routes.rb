Rails.application.routes.draw do
 
  root to: 'visitors#index'

  devise_for :admins, controllers: { registrations: 'admins/registrations', sessions: 'admins/sessions' }

  namespace :admins do
    get 'dashboard', to: 'dashboard#index'

    resources :sold_properties do 
      collection do 
        post 'import'
        get 'sample'
        delete 'remove'
      end
    end
    resources :admins
    resources :suburbs
    resources :intros
    resources :outros
    resources :templates
    resources :users, except: [:new, :create] do
      member do
        get 'contacts'
        get 'intros'
        post 'intros_save'
        get 'outros'
        post 'outros_save'
        get 'templates'
        post 'templates_save'
        get 'resend_password'
        post 'import_contacts'
        get 'import_contacts'
        post 'import'
      end
      collection do 
        get 'sample'
      end
    end
  end

  namespace :api do
    scope module: 'v1' do
      resources :agents, only: [] do
        collection do
          post 'profile'
          post 'subscription'
        end
      end
      resources :contacts, only: [:index, :create,:edit,:update,:destroy] do
        collection do 
          post :add_contacts_by_email
        end
        resources :properties, only: [:index, :create, :edit, :update, :destroy]
      end
      resources :messages, only: [:index, :update,:show]
      resources :intros, only: [:index] do 
        collection do 
          put :select_intros
        end
      end
      resources :outros, only: [:index] do 
        collection do 
          put :select_outros
        end
      end
      resources :templates, only: [:index]
      resources :sold_properties,only: [:index] do         
      end
      get 'sold_properties/contacts', :to => "sold_properties#contacts" 
    end
  end


  devise_for :users, controllers: {
    confirmations: 'authentications/confirmations',
    passwords: 'authentications/passwords',
    sessions: 'authentications/sessions'
  }

  devise_for :agents,controllers: {
    registrations: 'authentications/registrations',
    sessions: 'authentications/sessions',
    passwords: 'authentications/passwords'
  }
end