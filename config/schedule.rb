# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever
every 1.day, :at => '12:30 am' do
	rake "routine_task:delete_message_notification"
end

every 1.day, :at => '3:30 pm' do
	rake "routine_task:delete_message"
end

every :sunday, :at => '12pm' do # Use any day of the week or :weekend, :weekday
  rake "push:clean"
  rake "log:clear"
end