# config valid only for Capistrano 3.1
lock '3.3.3'

set :application, 'property-sms'
set :repo_url, 'git@bitbucket.org:applabsservice/property-sms-ruby.git'

set :use_sudo, true
set :stages, ["production"]
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, ENV['BRANCH'] || "master"
# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/ubuntu/propertysms'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/secrets.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :delayed_job_workers, 2

# set :delayed_job_roles, [:app]

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
set :pushd_pid_file, "#{current_path}/tmp/pids/push_daemon.pid" 

namespace :push do
  desc 'Start the push daemon'
  task :start do
    on roles(:app) do 
      execute "cd #{current_path} ; nohup bundle exec push #{stages} -p #{fetch(:pushd_pid_file)} >> #{current_path}/log/push.log 2>&1 &", :pty => false      
    end
    # run "cd #{current_path} ; nohup bundle exec push #{rails_env} -p #{pushd_pid_file} >> #{current_path}/log/push.log 2>&1 &", :pty => false
  end

  # desc 'Stop the push daemon'
  # task :stop, :roles => :web do
  #   # run "if [ -d #{current_path} ] && [ -f #{pushd_pid_file} ] && kill -0 `cat #{pushd_pid_file}`> /dev/null 2>&1; then kill -SIGINT `cat #{pushd_pid_file}` ; else echo 'push daemon is not running'; fi"
  # end

  # desc "Restart the push daemon"

  # task :restart do
  #   # stop
  #   start
  # end
end

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
      invoke 'delayed_job:restart'
      # invoke 'push:restart'
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
  
end

