$(document).on('ready page:load', function () {

    $('.fixed').click(function(event){
       if ($('.positions :checkbox:checked').length > 0){
            confirm("Are you sure?");
       } else{
        event.preventDefault();
        alert("Please select sold property to delete");
       }
    });
    
    $(":button.in_message").on("click", function() {
        var cursorPos = $("#" + $(this).data("input")).prop("selectionStart");
        var v = $("#" + $(this).data("input")).val();
        var textBefore = v.substring(0,  cursorPos );
        var textAfter  = v.substring( cursorPos, v.length );
        $("#" + $(this).data("input")).val( textBefore+ $(this).val() +textAfter );
        $("#character_count").val($("#" + $(this).data("input")).val().length);
    });

    $("#template_group").on("change", function() {$("#template_list").submit();});

    $("#intro_message").on("keyup", function() {$("#character_count").val($(this).val().length);});
    $("#outro_message").on("keyup", function() {$("#character_count").val($(this).val().length);});
    $("#template_message").on("keyup", function() {$("#character_count").val($(this).val().length);});

    $("#checkAll").on("change", function () {
        $("#checkbox-container input:checkbox").prop("checked", $(this).prop("checked"));
    });
    $("#checkbox-container input:checkbox").on("change", function () {
        $("#checkAll").prop("checked", ($('#checkbox-container input:checkbox:checked').length == $("#checkbox-container input:checkbox").length));
    });
    if ($("#intro_message").length) {
        $("#character_count").val($("#intro_message").val().length);
    }
    if ($("#outro_message").length) {
        $("#character_count").val($("#outro_message").val().length);
    }
    if ($("#template_message").length) {
        $("#character_count").val($("#template_message").val().length);
    }
    if ($('#token-container').length) {
        $('#token-container')
        .on("tokenfield:removedtoken", function (e) {
            $("#users option[value=" + e.attrs.value + "]").attr("selected", false);
            $("#all_users").prop("checked", false);
        })
        .tokenfield();
    }
    // if ($("#all_users").length) {
    //     $("#all_users").click( function() {
    //         $("#users option").prop("selected", $(this).prop("checked"));
    //         select_users();
    //     });
    // }
    if ($("#users").length) {
        $("#users").click(function() {
            select_users();
            $("#all_users").prop("checked", ($("#users option").length == $("#users option:selected").length));
        });
    }
    $('#sandbox-container input').datepicker({
        format: "yyyy/mm/dd",
         clearBtn: true,
         orientation: "top left",
         autoclose: true,
         todayHighlight: true
    });
});

select_users = function() {
    $("#token-container").tokenfield("destroy");
    $("#token-container").val("");
    $("#token-container")
    .tokenfield();
    $("#users :selected").each(function() {
        $("#token-container").tokenfield("createToken", {value: $(this).val(), label: $(this).text()});
    });
}

