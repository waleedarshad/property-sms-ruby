class CustomDeviseMailer < Devise::Mailer
  def headers_for(action, opts)
    if action == :confirmation_instructions
      if Rails.env.production?
        super.merge!({bcc: "admin@propertysms.com.au"})
      else
        super#.merge!({bcc: "waleedarshad72@gmail.com"})
      end
    else
      super
    end
  end
end
