class ApplicationMailer < ActionMailer::Base
  default from: "admin@propertysms.com.au"
  layout 'mailer'
end
