class Notifier < ApplicationMailer
	default from: "admin@propertysms.com.au"

	def send_import_contacts_instruction(user)
    @user = user
    attachments['sample.xlsx'] = File.read("#{Rails.root}/public/sample.xlsx",)
		mail(:to => @user.email, :subject => "PropertySMS: Instructions for Importing Contacts")
	end
end
