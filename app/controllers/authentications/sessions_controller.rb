class Authentications::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]

  skip_before_filter :verify_signed_out_user

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out

  def create
    resource = warden.authenticate!(:scope => resource_name, recall: 'authentications/sessions#failure')
    return sign_in_and_redirect(resource_name, resource)
  end

  def sign_in_and_redirect(resource_or_scope, resource=nil)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    resource ||= resource_or_scope
    sign_in(scope, resource) unless warden.user(scope) == resource
    if params[:agent][:udId_android].present? and params[:agent][:device].present?
      # For Android Devices
      puts "-------------------------------Sign in as android-----------------------------------------------"
      puts "before Sign In==Android==============#{resource.udId_android}============================"
      User.where(:udId_android => params[:agent][:udId_android]).update_all(:udId_android=>nil)  
      resource.update_column(:udId_android,params[:agent][:udId_android])
      resource.update_column(:device,params[:agent][:device])
      puts "after Sign IN==Android=========================#{resource.udId_android}====================="
      puts "---------------------------------------------------------------------------------"
    else
      # For IOS Devices
      puts "---------------------------------Sign in as IOS---------------------------------------------"
      puts "before Sign In==IOS==============#{resource.ud_ID}============================"
      User.where(:ud_ID => params[:agent][:ud_ID]).update_all(:ud_ID=>nil)   
      if params[:agent][:ud_ID].present?  and params[:agent][:ud_ID] != resource.ud_ID 
        resource.update_column(:ud_ID,params[:agent][:ud_ID] )
      end
      puts "after Sign IN==IOS=========================#{resource.ud_ID}====================="
      puts "---------------------------------------------------------------------------------"
    end

    return render :json => resource.as_json.merge!(:subscription_expired => subscription_expired?(resource) )
  end

  def failure
    return render_error("Login failed", 401, 401)
  end


  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    if current_agent 
      if params[:device]=="android"
        current_agent.update_column(:udId_android,nil)
      else
        current_agent.update_column("ud_ID",nil)
      end
    end

    
    signed_out = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    set_flash_message :notice, :signed_out if signed_out

    respond_to do |format|
      format.html { redirect_to redirect_path }
      format.json { render :json => {:success => true} }
    end
  end

  # protected

  # You can put the params you want to permit in the empty array.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end
end
