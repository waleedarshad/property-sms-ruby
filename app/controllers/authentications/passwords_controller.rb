class Authentications::PasswordsController < Devise::PasswordsController
  # respond_to :html, :json
  # GET /resource/password/new
  # respond_to :json
  def new
    self.resource = resource_class.new
  end

  # POST /resource/password
  def create
    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?
    if successfully_sent?(resource)
      return render :json => {:message => "Reset password instruction has been sent to your email"}
      # respond_with(json: {:message => "Reset password instruction has been sent to your email"}, location: after_sending_reset_password_instructions_path_for(resource_name))
    elsif resource.errors.present?
      # render_error(resource.errors.full_messages.to_sentence, 422, 422)
       return render :json => resource.errors ,:status=>:unprocessable_entity 
    end
  end

  # GET /resource/password/edit?reset_password_token=abcdef
  # def edit
  #   super
  # end

  # PUT /resource/password
  def update
    self.resource = resource_class.reset_password_by_token(resource_params)
    yield resource if block_given?

    if resource.errors.empty?
        flash[:notice] = "Password Changed Successfully "
        respond_with resource, location: root_path
    else
      respond_with resource
    end

    # self.resource = resource_class.reset_password_by_token(resource_params)
    # yield resource if block_given?

    # if resource.errors.empty?
    #   resource.unlock_access! if unlockable?(resource)
    #   if Devise.sign_in_after_reset_password
    #     flash_message = resource.active_for_authentication? ? :updated : :updated_not_active
    #     set_flash_message(:notice, flash_message) if is_flashing_format?
    #     sign_in(resource_name, resource)
    #     respond_with resource, location: root_path
    #   else
    #     set_flash_message(:notice, :updated_not_active) if is_flashing_format?
    #     respond_with resource, location: new_session_path(resource_name)
    #   end
    # else
    #   respond_with resource
    # end
  end

  # protected

  # def after_sending_reset_password_instructions_path_for(resource_name)
  #   # root_path
  #   binding.pry
  # end
end
