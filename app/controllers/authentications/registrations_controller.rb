class Authentications::RegistrationsController < Devise::RegistrationsController

	def create
		@user = User.new(users_params)
        respond_to do |format|
          if @user.save
             format.json { render :json=>@user.as_json.merge({message: "Confirmation instructions have been sent to your email. Please check your junk folder if it is not in your inbox."})}
          else

            if @user.errors.messages[:email].present? and @user.errors.messages[:email][0] == "has already been taken"
              @user.errors.messages.delete(:email) 
              @user.errors.messages.merge!("this email" => ["address is already in use."])
            end
            format.json { render :json => @user.errors ,:status=>:unprocessable_entity }
          end
        end
	end

  def update
    @user = User.find(params[:id])
        respond_to do |format|
          if @user.update_attributes(users_params)
             format.json { render :json=>@user.as_json}
          else
            format.json { render :json => @user.errors ,:status=>:unprocessable_entity }
          end
        end
  end

	private
      def users_params
        params.require(:agent).permit(:email, :first_name,:last_name,:company,:mobile_number, :password, :password_confirmation, :customer_id)
      end

end