class Api::V1::OutrosController < ApplicationController
	before_filter :authenticate_agent_from_token!

def index
  
  # outros = Outro.where("outros_users.user_id = ? ",current_user.id).joins("INNER JOIN outros_users on outros.id = outros_users.outro_id ") | Outro.where("all_users =?",true)
  outros = current_user.outros | Outro.where("all_users =?",true)
  user_selected = current_user.outros_admin_false.pluck(:outro_id).uniq

  outros = outros.sort_by {|obj| obj.id}

  respond_to do |format|
    format.json { render :json => outros.uniq.as_json({:user_id => current_user.id}).push(user_selected)}
  end
end

def select_outros	
	respond_to do |format|
		if params[:admin] and params[:outro_id]
			user_outros = current_user.outros_users_user.where(:outro_id => params[:outro_id]).first
			if params[:admin] == "true"
				if !user_outros.nil?
					user_outros.destroy
				end
			else
				if user_outros.nil?
					current_user.outros_users_user.create(outro_id: params[:outro_id], admin: false)
				end
			end
				format.json { render :json => {:success => true}}
			else
				format.json { render :json => {:error => true},:status =>  :unprocessable_entity}
			end
	end	
end

end