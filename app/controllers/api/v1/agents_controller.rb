class Api::V1::AgentsController < ApplicationController
	before_filter :authenticate_agent_from_token!

  def profile
    respond_to do |format|
      if current_user.update_attributes(agent_params)
         format.json { render :json => current_user.as_json.merge({message: "Agent has successfully been updated."})}
      else
        format.json { render :json => current_user.errors, :status => :unprocessable_entity }
      end
    end
  end

  def subscription
    respond_to do |format|
         params[:agent][:subscription_expired_at] = Date.today + 1.month
      if current_user.update_attributes(agent_params)
        format.json { render :json => current_user.as_json(:subscription => true).merge!(subscription_expired: subscription_expired?(current_user))}
      else
        format.json { render :json => current_user.errors, :status => :unprocessable_entity }
      end
    end
  end

private
  def agent_params
  	params.require(:agent).permit(
      :first_name,
      :last_name,
      :email,
      :company,
      :mobile_number,
      :customer_id,
      :terms_accepted,
      :subscription,
      :subscription_expired_at
    )
  end
end