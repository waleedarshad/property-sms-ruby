class Api::V1::IntrosController < ApplicationController
	before_filter :authenticate_agent_from_token!

def index
  
  # intros = Intro.joins("INNER JOIN intros_users on intros.id = intros_users.intro_id ").where("intros_users.user_id = ? ",current_user.id) | Intro.where("all_users =?",true)
  intros = current_user.intros | Intro.where("all_users =?",true)
  user_selected = current_user.intros_admin_false.pluck(:intro_id).uniq

  intros = intros.sort_by {|obj| obj.id}
  respond_to do |format|
    format.json { render :json => intros.uniq.as_json({:user_id => current_user.id}).push(user_selected)}
  end
end

def select_intros
	respond_to do |format|
		if params[:admin] and params[:intro_id]
			user_intros = current_user.intros_users_user.where(:intro_id => params[:intro_id]).first
			if params[:admin] == "true"
				if !user_intros.nil?
					user_intros.destroy
				end
			else
				if user_intros.nil?
					current_user.intros_users_user.create(intro_id: params[:intro_id], admin: false)
				end
			end
			format.json { render :json => {:success => true}}
		else
			format.json { render :json => {:error => true},:status =>  :unprocessable_entity}
		end
	end
end

end
