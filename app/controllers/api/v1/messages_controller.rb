class Api::V1::MessagesController < ApplicationController

	before_filter :authenticate_agent_from_token!

	def index
    contacts_ids =  current_user.contacts.pluck(:id) 
    messages = Message.where(:sent => false,:contact_id => contacts_ids).group("contact_id").includes(:contact,:sold_property,message_properties: [:property])
    contacts = Array.new
    messages.each do |message_|      
      
      c = message_.contact
      
      last_message = Message.where('contact_id = ? AND sent = true', c.id).order('sent_date_at DESC').limit(1).first
      last_message_sent_at = (last_message.nil?) ? '' : last_message.sent_date_at

      month_messages = Message.where('contact_id = ? AND sent = true', c.id).where('sent_date_at >= ?', (Date.today - 1.months)).count

      last_message = Message.where('contact_id = ? AND sent = false', c.id).order('created_at DESC').limit(1).first
      last_message_created_at = (last_message.nil?) ? '' : last_message.created_at.strftime('%Y-%m-%d')

      if message_.contact.present? and message_.sold_property.present?
        short_distance_property = message_.message_properties.sort_by(&:distance).first
        if short_distance_property.present?
            is_property_address_same =  false
            street = message_.sold_property.street_name
            if street
              street_abbr = ApplicationController.helpers.street_abbrevitation
              street_abbr_keys = street_abbr.keys 
              street.downcase! unless street.downcase! == nil 
              l_street = street
              street_componenet = l_street.squish!.split(' ')   
              street_componenet.each do |street_word|     
                if street_abbr_keys.include?(street_word)
                  l_street.sub! /\b#{street_word}\b/,street_abbr[street_word].titleize     
                  # street = l_street.titleize
                end
              end
              street = l_street.sub! /.*/, l_street.titleize  
              if street == short_distance_property.property.street
                is_property_address_same =  true
              end
            end
          message = {
            'message_id' => message_.id,
            'contact' => message_.contact.attributes.reject { |k, v| v.nil? },
            'property' => short_distance_property.property.attributes.reject { |k, v| v.nil? },
            'sold_property' => message_.sold_property.attributes.reject { |k, v| v.nil? },
            'distance' => short_distance_property.distance,
            'last_message_sent_at' => last_message_sent_at,
            'month_messages' => month_messages,
            'last_message_created_at' => last_message_created_at,
            'template' => message_.template,
            'is_property_address_same' => is_property_address_same,
            'subscription_expired' => subscription_expired?(current_user) 

          }
        end
      end
      contacts << message unless message.blank? 
    end
    respond_to do |format|
      format.json { render :json => contacts}
    end
	end

  def update 
    @message = Message.find(params[:id]) 

    respond_to do |format|
      if @message
      	message_params = {
      		message_body: params[:message][:message_body],
      		sent: true,
      		sent_date_at: Time.now
      	}
        if @message.update_attributes(message_params)
          format.json { render :json => @message.as_json.merge({message: "Message successfully updated"})}
        else
          format.json { render :json => @message.errors , :status => :unprocessable_entity }
        end
      else
        format.json { render :json => {error: "No Message Found", :status => :unprocessable_entity} }
      end
    end   
  end
end