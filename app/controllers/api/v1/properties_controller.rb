class Api::V1::PropertiesController < ApplicationController
	before_filter :authenticate_agent_from_token!

  # def index
  #   contact = current_user.contacts.find(params[:id])

  #   if !contact.nil?
      
  #   else
  #     render :json => {error: "No contact found", :status => :unprocessable_entity}
  #   end
  # end

  def create
  	contact = Contact.find_by(id: params[:contact_id])
    if !contact.nil?
      @property = contact.properties.new(property_params)
      respond_to do |format|
        if @property.save
           format.json { render :json => @property.as_json.merge({message: "Property successfully created"})}
        else
          format.json { render :json => @property.errors ,:status=>:unprocessable_entity }
        end
      end
    else
      render :json => {error: "No contact found", :status => :unprocessable_entity}
    end
  end


  def update
    @contact = current_user.contacts.find_by_id(params[:contact_id])
    if @contact
      @property = @contact.properties.find_by_id(params[:id])
      unless @property
        @message = "No Property Found"
      end
    else
      @message = "No Contact Found"
    end

    respond_to do |format|
      if @property
        if @property.update_attributes(property_params)
           format.json { render :json => @property.as_json.merge({message: "Property successfully Updated"})}
        else
          format.json { render :json => @property.errors ,:status => :unprocessable_entity }
        end
      else
        format.json { render :json => {error: @message}, :status => :unprocessable_entity }
      end
    end   
  end

  def destroy
    @contact = current_user.contacts.find_by_id(params[:contact_id])
    if @contact
      @property = @contact.properties.find_by_id(params[:id])
      unless @property
        @message = "No Property Found"
      end
    else
      @message = "No Contact Found"
    end

    respond_to do |format|
      if @property
          @id = @property.id
        if @property.delete
          # Message.where(:property_id => @id).delete_all
          MessageProperty.where(:property_id => @id).delete_all
          format.json { render :json=>{message: "Property successfully deleted"}}
        else
          format.json { render :json => @property.errors ,:status=>:unprocessable_entity }
        end
      else
        format.json { render :json=>{error: @message},:status=>:unprocessable_entity}
      end
    end     
  end

private
  def property_params
  	params.require(:property).permit(
      :unit_number,
      :house_number,
      :street,
      :investment_property,
      :suburb,
      :state,
      :postcode,
      :sold
    )
  end
end