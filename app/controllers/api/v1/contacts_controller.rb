class Api::V1::ContactsController < ApplicationController
  before_filter :authenticate_agent_from_token!

  def index
    contacts_ = current_user.contacts.includes(:properties)
    # .collect{|contact| contact.properties.map{|property| property.attributes.merge(contact.attributes) } }

    contacts = Array.new

    contacts_.each do |c|
      last_message = Message.where('contact_id = ? AND sent = true', c.id).order('sent_date_at DESC').limit(1).first
      last_message_sent = (last_message.nil?) ? '' : last_message.sent_date_at

      month_messages = Message.where('contact_id = ? AND sent = true', c.id).where('sent_date_at >= ?', (Date.today - 1.months)).count

      contact = Hash.new
      contact[:id] = c.id
      contact[:first_name] = c.first_name
      contact[:last_name] = c.last_name
      contact[:mobile_number] = c.mobile_number
      contact[:partner_first_name] = c.partner_first_name
      contact[:partner_last_name] = c.partner_last_name
      contact[:partner_mobile_number] = c.partner_mobile_number
      contact[:last_message_sent] = last_message_sent
      contact[:month_messages] = month_messages
      contact[:user_id] = current_user.id

      contact[:unverfied_properties_count] = c.properties.unverified.count

      contact[:properties] = c.properties
      contacts << contact
    end

    respond_to do |format|
      format.json { render :json => contacts}
    end
  end


  def create
    
    @created_contacts = 0

    respond_to do |format|
      if params[:contact].kind_of?(Array)

        params[:contact].each do |contact|
        @contact = Contact.unscoped.where(:user_id => current_user.id).find_by({mobile_number: contact[:mobile_number]})

          if @contact and @contact.deleted?
            @contact.update_columns({:deleted => false,first_name: contact[:first_name],last_name: contact[:last_name],mobile_number: contact[:mobile_number],:partner_first_name => contact[:partner_first_name],:partner_last_name => contact[:partner_last_name],:partner_mobile_number => contact[:partner_mobile_number]})
            create_properties(@contact,contact[:properties]) if contact[:properties].present?
            @created_contacts +=1
          else
            unless @contact.present? # do nothing if contact already exisits
              @contact = current_user.contacts.build({first_name: contact[:first_name],last_name: contact[:last_name],mobile_number: contact[:mobile_number],:partner_first_name => contact[:partner_first_name],:partner_last_name => contact[:partner_last_name],:partner_mobile_number => contact[:partner_mobile_number]}) 
              if @contact.save
                create_properties(@contact,contact[:properties]) if contact[:properties].present?
                @created_contacts +=1
              end  
            end
          end
        end

        if @created_contacts == 0
           message = "Contact already exists"
        else
          message = "#{@created_contacts} contacts successfully created"
        end
        format.json { render :json=>{message: message}}
      else
        @contact = Contact.unscoped.where(:user_id => current_user.id).find_by({mobile_number: params[:contact][:mobile_number]})
        if @contact and @contact.deleted?
            format.json { render :json=>{id: @contact.id ,message: "You have already deleted account with this Mobile Number, Do you want to recreate it?"}}
        else
          unless @contact.present?
            @contact = current_user.contacts.build({first_name: params[:contact][:first_name],last_name: params[:contact][:last_name],mobile_number: params[:contact][:mobile_number],:partner_first_name => params[:contact][:partner_first_name],:partner_last_name => params[:contact][:partner_last_name],:partner_mobile_number => params[:contact][:partner_mobile_number]})
            if @contact.save
                format.json { render :json=>@contact.as_json.merge({message: "Contact successfully created"})}
            else
              format.json { render :json => @contact.errors ,:status=>:unprocessable_entity }
            end
          else
            format.json { render :json=>{message: "contact already exists"}}
          end
        end
      end
    end
  end

  def create_properties(contact,properties)
    properties.each do |property|
      contact.properties.create(unit_number: property[:unit_number],house_number: property[:house_number],street: property[:street],suburb: property[:suburb],state: property[:state],postcode: property[:postcode])
    end
  end

  def destroy
    @contact = current_user.contacts.find_by_id(params[:id])
    respond_to do |format|
      if @contact
        if @contact.update_column(:deleted , true)
          @contact.properties.delete_all
          format.json { render :json=>{message: "Contact deleted successfully"}}
        else
          format.json { render :json => @contact.errors ,:status=>:unprocessable_entity }
        end
      else
        format.json { render :json=>{error: "No Contact Found",:status=>:unprocessable_entity} }
      end
    end   
  end

  def update 
    @contact = current_user.contacts.unscoped.find_by_id(params[:id]) 
    if @contact.deleted?
      @contact.update_column(:deleted , false)
      message = "Contact successfully created"
    end
    respond_to do |format|
      if @contact
        if @contact.update_attributes(contact_params)
          format.json { render :json=>@contact.as_json.merge({message: message ? message : "Contact successfully updated"})}
        else
          format.json { render :json => @contact.errors ,:status=>:unprocessable_entity }
        end
      else
        format.json { render :json=>{error: "No Contact Found",:status=>:unprocessable_entity} }
      end
    end   
  end

  def add_contacts_by_email
   Notifier.send_import_contacts_instruction(current_user).deliver_now
   render :json=>{message: "Import Contact Instruction is sent you your email address, please follow instruction to proceed."}
  end
private
  def contact_params
  	params.require(:contact).permit(:first_name, :last_name, :mobile_number, :partner_first_name, :partner_last_name, :partner_mobile_number)
  end
end

