class Api::V1::SoldPropertiesController < ApplicationController

	before_filter :authenticate_agent_from_token!

	def index
		sold_properties = SoldProperty.near([params[:latitude], params[:longitude]], 20, :units => :km).limit(20)

		properties = Array.new
		sold_properties.each do |sold_property|
			property = {}
			property[:address_id] = sold_property.address_id
			property[:postcode] = sold_property.postcode
			property[:state] = sold_property.state
			property[:property_type] = sold_property[:property_type]
			property[:contract_date] = sold_property[:contract_date]
			property[:transaction_date] = sold_property[:transaction_date]
			property[:areasize] = sold_property[:areasize]
			property[:bedrooms] = sold_property[:bedrooms]
			property[:baths] = sold_property[:baths]
			property[:parking] = sold_property[:parking]
			property[:eventtype_code] = sold_property[:eventtype_code]
			property[:event_price] = sold_property[:event_price]
			property[:event_date] = sold_property[:event_date]
			property[:suburb] = sold_property[:suburb]
			property[:street_type] = sold_property[:street_type]
			property[:street_name] = sold_property[:street_name]
			property[:street_number] = sold_property[:street_number]
			property[:flat_number] = sold_property[:flat_number]
			property[:distance] = sold_property[:distance].round(2).to_s + "km"
			properties << property
		end

		respond_to do |format|
			format.json { render :json => properties}
		end
	end

	def contacts
		respond_to do |format|
			range = params[:range]
			if range
				s = Suburb.where("close_range =?",range).pluck(:address)
				contacts = Property
				.near([params[:latitude], params[:longitude]], 20, :units => :km)
				.where("sp.suburb in (?)", s)
				.joins("
					INNER JOIN sold_properties sp ON properties.suburb = sp.suburb AND 
						sp.state = properties.state AND sp.postcode = properties.postcode 
					INNER JOIN contacts c ON c.id = properties.contact_id").
				select("
					c.id AS contact_id,
					c.first_name,
					c.last_name,
					c.mobile_number,
					c.user_id,
					sp.id AS sold_property_id,
					c.created_at,
					c.updated_at,
					properties.*
				").group('properties.id')

				formated_contacts = []
				contact = Hash.new
				a_properties = Array.new
				ids = contacts.map(&:contact_id)

				ids.uniq.each do |id|
					selected_contacts = contacts.select{ |hash| hash[:contact_id] ==id }

					contact[:id] = selected_contacts.first.contact_id
					contact[:first_name] = selected_contacts.first.first_name rescue nil
					contact[:last_name] = selected_contacts.first.last_name rescue nil
					contact[:mobile_number] = selected_contacts.first.mobile_number rescue nil
					
					selected_contacts.each do |hash|
						h = {}
						h[:id] = hash.id
						h[:postcode] = hash.postcode
						h[:state] = hash.state
						h[:suburb] = hash[:suburb]
						h[:unit_number] = hash[:unit_number]
						h[:house_number] = hash[:house_number]
						h[:street] = hash[:street]
						h[:investment_property] = hash[:investment_property] ? true : false
						h[:sold] = hash[:sold] ? true : false
						h[:sold_property_id] = hash[:sold_property_id]
						h[:distance] = hash[:distance].round(2).to_s + 'km'
						a_properties << h
					end
					contact[:properties] = a_properties
					formated_contacts << contact
					contact = {}
					a_properties = []
				end

				format.json { render :json => formated_contacts}
			else
				format.json { render :json => {:message => "No Range param given"}}
			end
		end
	end
end