class Admins::UsersController < ApplicationController
  layout 'admin'

  before_filter :authenticate_admin!
  before_action :set_user, only: [
    :show, :edit, :update, :destroy,
    :contacts,
    :intros, :intros_save,
    :outros, :outros_save,
    :templates, :templates_save,
    :resend_password
  ]

  # GET /users
  # GET /users.json
  def index
    if params[:s].nil?
      @users = User.order(:first_name).paginate(:page => params[:page], :per_page => 20)
    else
      @users = User.where("first_name LIKE '%#{params[:s]}%' OR last_name LIKE '%#{params[:s]}%' OR company LIKE '%#{params[:s]}%' OR email LIKE '%#{params[:s]}%'").order(:first_name).paginate(:page => params[:page], :per_page => 20)
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/1/edit
  def edit
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update_attributes(user_params)
        @user.update_column(:email, params[:user][:email])
        format.html { redirect_to admins_users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admins_users_path, notice: 'User was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  # GET /users/1/contacts
  # GET /users/1/contacts.json
  def contacts
  end

  def import_contacts

      @file = params[:file].path if params[:file]
      @contact_array = [] unless @contact_array
      if request.post? 
        if (params[:file].content_type == "application/vnd.ms-excel" || params[:file].content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") and validate_header?(@file)
          @contact_array =  User.import_contacts(params) 
        else
          flash[:notice] = "Not a valid format or file is empty"
        end
      end
  end

  def import
    get_prev_max_imported_id = User.import(params)
    Property.create_coordinate(get_prev_max_imported_id)
    # MessageJob.new.async.perform(for_contact_property_coordinate: get_prev_max_imported_id)
    redirect_to admins_users_url ,:notice => "Successfully saved"
  end

  def sample
    respond_to do |format| 
       format.xlsx {render xlsx: 'sample',filename: "sample.xlsx"}
    end
    # respond_to do |format| 
    #   format.xlsx do
    #       response.headers['Content-Type'] = 'text/xlsx'
    #       response.headers['Content-Disposition'] = 'attachment; filename=sample.xlsx'    
    #       render "sample.xlsx.erb"
    #   end
    # end
  end

  # GET /users/1/intros
  # GET /users/1/intros.json
  def intros
    @intros = Intro.all
  end

  # POST /users/1/intros_save
  # POST /users/1/intros_save.json
  def intros_save
    @user.intros.destroy_all
    if !params[:intro].nil? && !params[:intro].blank?
      params[:intro].each do |intro_id|
        intro = Intro.find(intro_id)
        @user.intros << intro
      end
    end
    redirect_to intros_admins_user_path, notice: 'Intros was successfully updated.'
  end

  # GET /users/1/outros
  # GET /users/1/outros.json
  def outros
    @outros = Outro.all
  end

  # POST /users/1/outros_save
  # POST /users/1/outros_save.json
  def outros_save
    @user.outros.destroy_all
    if !params[:outro].nil? && !params[:outro].blank?
      params[:outro].each do |outro_id|
        outro = Outro.find(outro_id)
        @user.outros << outro
      end
    end
    redirect_to outros_admins_user_path, notice: 'Outros was successfully updated.'
  end

  # GET /users/1/templates
  # GET /users/1/templates.json
  def templates
    @templates = Template.all
  end

  # POST /users/1/templates_save
  # POST /users/1/templates_save.json
  def templates_save
    @user.templates.destroy_all
    if !params[:template].nil? && !params[:template].blank?
      params[:template].each do |template_id|
        template = Template.find(template_id)
        @user.templates << template
      end
    end
    redirect_to templates_admins_user_path, notice: 'Templates was successfully updated.'
  end

  def resend_password
    @user.send_reset_password_instructions
    flash[:notice] =  "Password instructions has successfully been sent to user's email address."
    redirect_to admins_users_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :company, :mobile_number, :email)
    end

    def validate_header?(params)
      User.valid_header?(params)
    end
end