class Admins::SoldPropertiesController < ApplicationController
  before_filter :authenticate_admin!
	layout 'admin'
  def index
  	if params[:s].blank?
  	  @sold_properties = SoldProperty.paginate(:page => params[:page], :per_page => 20)
  	else
  	  @sold_properties = SoldProperty.where("address_id LIKE  ? OR
        state LIKE ? OR 
        property_type LIKE ? OR 
        contract_date LIKE ? OR 
        transaction_date LIKE ? OR
        postcode LIKE ? OR 
        areasize LIKE ? OR 
        bedrooms LIKE ? OR 
        baths LIKE ? OR 
        parking LIKE ? OR 
        eventtype_code LIKE ? OR 
        event_price LIKE ? OR 
        event_date LIKE ? OR 
        suburb LIKE ? OR 
        street_type LIKE ? OR 
        street_name LIKE ? OR 
        street_number LIKE ? OR 
        flat_number LIKE ? ",
        "%#{params[:s]}%" ,"%#{params[:s]}%" ,"%#{params[:s]}%" ,"%#{params[:s]}%" ,
        "%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%",
        "%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%",
        "%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%","%#{params[:s]}%" ).paginate(:page => params[:page], :per_page => 20)
  	end
    @property_count = SoldProperty.count
  end

  def import
  	require 'roo'
  	@file = params[:file].path if params[:file]
  	if (params[:file].content_type == "application/vnd.ms-excel" or params[:file].content_type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") and validate_header?(@file) 
			previous_sold_property = SoldProperty.import(@file)
      if previous_sold_property

        SoldProperty.create_coordinate(previous_sold_property)  # make coordinate if for some reason not make
        SoldProperty.messages_queue(previous_sold_property) # send notification after 2 mint

        # MessageJob.new.async.perform(for_property_coordinate: previous_sold_property)
        # MessageJob.new.async.later(120, for_property_sms: previous_sold_property) 
      end
	  	flash[:notice] = "File Imported Successfully"
	  else
	  	flash[:notice] ="File formate is not valid"
		end
		redirect_to admins_sold_properties_path
 	end

  def sample
      respond_to do |format| 
         format.xlsx {render xlsx: 'sample',filename: "sample.xlsx"}
      end
    end
  
    def remove
      ids = params[:sold_properties_ids]
      properties = SoldProperty.where(:id => ids)
      if properties.count > 0
        properties.delete_all
        flash[:notice] = "SoldProperties Delete Successfully"
      else
        flash[:notice] = "Something went Wrong, Please try later thank you!"
      end
      redirect_to admins_sold_properties_path
    end
 private
 	def validate_header?(params)
 		SoldProperty.valid_header?(params)
 	end
end
