class Admins::IntrosController < ApplicationController
  layout 'admin'

  before_filter :authenticate_admin!
  before_action :set_intro, only: [:show, :edit, :update, :destroy]

  # GET /intros
  # GET /intros.json
  def index
    if params[:s].nil?
      @intros = Intro.paginate(:page => params[:page], :per_page => 20)
    else
      @intros = Intro.where("message LIKE '%#{params[:s]}%'").paginate(:page => params[:page], :per_page => 20)
    end
  end

  # GET /intros/1
  # GET /intros/1.json
  def show
  end

  # GET /intros
  def new
     @intro = Intro.new
     @users = User.order(:first_name)
  end

  # GET /intros/1/edit
  def edit
     @users = User.order(:first_name)
  end

  # PATCH/POST /intros/1
  # PATCH/POST /intros/1.json
  def create
    @intro = Intro.new(intro_params)
    @users = User.order(:first_name)

    respond_to do |format|
      if @intro.save
        save_selected_users

        format.html { redirect_to admins_intros_path, notice: 'Intro was successfully created.' }
        format.json { render :show, status: :ok, location: @intro }
      else
        format.html { render :edit }
        format.json { render json: @intro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /intros/1
  # PATCH/PUT /intros/1.json
  def update
    @users = User.order(:first_name)
    respond_to do |format|
      if @intro.update(intro_params)
        save_selected_users

        format.html { redirect_to admins_intros_path, notice: 'Intro was successfully updated.' }
        format.json { render :show, status: :ok, location: @intro }
      else
        format.html { render :edit }
        format.json { render json: @intro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /intros/1
  # DELETE /intros/1.json
  def destroy
    @intro.destroy
    respond_to do |format|
      format.html { redirect_to admins_intros_url, notice: 'Intro was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def save_selected_users
      @intro.users.destroy_all
      if !params[:users].nil? && !params[:users].blank?
        params[:users].each do |user_id|
          user = User.find(user_id)
          @intro.users << user
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_intro
      @intro = Intro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def intro_params
      params.require(:intro).permit(:message, :all_users)
    end
end