class Admins::AdminsController < ApplicationController
  layout 'admin'
  before_action :authenticate_admin!
  before_action :set_admin ,:only => [:show,:edit,:update,:destroy]
  def index
    if params[:s].nil?
      @admins = Admin.paginate(:page => params[:page], :per_page => 20)
    else
      @admins = Admin.where("email LIKE '%#{params[:s]}%'").paginate(:page => params[:page], :per_page => 20)
    end
  end

  def new
    @admin = Admin.new
  end

  def show
  end

  def create
    @admin = Admin.new(admin_params)

    respond_to do |format|

      if @admin.save
        format.html { redirect_to admins_admins_path, notice: 'Admin created Successfully' }
      else
        format.html { render :edit }
      end
    end  
  end

  def edit
  end

  def update
    respond_to do |format|
      if @admin.update(admin_params)
        format.html { redirect_to admins_admins_path, notice: 'Admin updated Successfully.' }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @admin.destroy
    respond_to do |format|
      format.html { redirect_to admins_admins_url, notice: 'Admin deleted Successfully.' }
    end
  end

private
  def set_admin
    @admin = Admin.find(params[:id])
  end

  def admin_params
    params.require(:admin).permit(:email,:password,:status)
  end
end
