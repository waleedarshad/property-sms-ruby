class Admins::SuburbsController < ApplicationController
	  layout 'admin'

	  before_filter :authenticate_admin!
	  before_action :set_suburb, only: [:show, :edit, :update, :destroy]

	  def index
	  	if params[:s].nil?
	  	  @suburbs = Suburb.paginate(:page => params[:page], :per_page => 20)
	  	else
	  	  @suburbs = Suburb.where("state LIKE ? OR address LIKE ? OR close_range LIKE ? OR medium_range LIKE ?","%#{params[:s]}%" ,"%#{params[:s]}%" ,"%#{params[:s]}%" ,"%#{params[:s]}%" ).paginate(:page => params[:page], :per_page => 20)
	  	end
	  end

	  def show
	  end

	  def new
	     @suburb = Suburb.new
	  end

	  def edit
	  end

	  def create
	    @suburb = Suburb.new(suburb_params)
	    
	    respond_to do |format|
	      if @suburb.save
	        format.html { redirect_to admins_suburbs_path, notice: 'Suburb was successfully created.' }
	      else
	        format.html { render :edit }
	      end
	    end
	  end

	  def update

	    respond_to do |format|

	      if @suburb.update(suburb_params)
	        format.html { redirect_to admins_suburbs_path, notice: 'Suburb was successfully updated.' }
	      else
	        format.html { render :edit }
	      end
	    end
	  end

	  def destroy
	    @suburb.destroy
	    respond_to do |format|
	      format.html { redirect_to admins_suburbs_url, notice: 'Suburb was successfully deleted.' }
	    end
	  end

	  private

	    # Use callbacks to share common setup or constraints between actions.
	    def set_suburb
	      @suburb = Suburb.find(params[:id])
	    end

	    # Never trust parameters from the scary internet, only allow the white list through.
	    def suburb_params
	      params.require(:suburb).permit(:state, :address,:close_range,:medium_range)
	    end
	end
