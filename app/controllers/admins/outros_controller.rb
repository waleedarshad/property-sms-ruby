class Admins::OutrosController < ApplicationController
  layout 'admin'

  before_filter :authenticate_admin!
  before_action :set_outro, only: [:show, :edit, :update, :destroy]

  # GET /outros
  # GET /outros.json
  def index
    if params[:s].nil?
      @outros = Outro.paginate(:page => params[:page], :per_page => 20)
    else
      @outros = Outro.where("message LIKE '%#{params[:s]}%'").paginate(:page => params[:page], :per_page => 20)
    end
  end

  # GET /outros/1
  # GET /outros/1.json
  def show
  end

  # GET /outros
  def new
     @outro = Outro.new
     @users = User.order(:first_name)
  end

  # GET /outros/1/edit
  def edit
     @users = User.order(:first_name)
  end

  # PATCH/POST /outros/1
  # PATCH/POST /outros/1.json
  def create
    @outro = Outro.new(outro_params)
     @users = User.order(:first_name)

    respond_to do |format|
      if @outro.save
        save_selected_users

        format.html { redirect_to admins_outros_path, notice: 'Outro was successfully created.' }
        format.json { render :show, status: :ok, location: @outro }
      else
        format.html { render :edit }
        format.json { render json: @outro.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /outros/1
  # PATCH/PUT /outros/1.json
  def update
     @users = User.order(:first_name)
    respond_to do |format|
      if @outro.update(outro_params)

        save_selected_users
        format.html { redirect_to admins_outros_path, notice: 'Outro was successfully updated.' }
        format.json { render :show, status: :ok, location: @outro }
      else
        format.html { render :edit }
        format.json { render json: @outro.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /outros/1
  # DELETE /outros/1.json
  def destroy
    @outro.destroy
    respond_to do |format|
      format.html { redirect_to admins_outros_url, notice: 'Outro was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def save_selected_users
      @outro.users.destroy_all
      if !params[:users].nil? && !params[:users].blank?
        params[:users].each do |user_id|
          user = User.find(user_id)
          @outro.users << user
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_outro
      @outro = Outro.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def outro_params
      params.require(:outro).permit(:message, :all_users)
    end
end