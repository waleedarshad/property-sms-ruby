class Admins::TemplatesController < ApplicationController
  layout 'admin'

  before_filter :authenticate_admin!
  before_action :set_template, only: [:show, :edit, :update, :destroy]

  # GET /templates
  # GET /templates.json
  def index
    if params[:s].blank? and params[:g].blank?
      @templates = Template.paginate(:page => params[:page], :per_page => 20)
    else
      if params[:g] == "All"
        if params[:s].blank? 
          @templates = Template.paginate(:page => params[:page], :per_page => 20)
        else
          @templates = Template.where("message LIKE '%#{params[:s]}%'").paginate(:page => params[:page], :per_page => 20)      
        end
      else
        if params[:s].blank?
          @templates = Template.where("`group`='#{params[:g]}'").paginate(:page => params[:page], :per_page => 20)
        else
          @templates = Template.where("`group`='#{params[:g]}' AND message LIKE '%#{params[:s]}%'").paginate(:page => params[:page], :per_page => 20)  
        end
      end
    end
  end

  # GET /templates/1
  # GET /templates/1.json
  def show
  end

  # GET /templates
  def new
     @template = Template.new
     @users = User.order(:first_name)
  end

  # GET /templates/1/edit
  def edit
     @users = User.order(:first_name)
  end

  # PATCH/POST /templates/1
  # PATCH/POST /templates/1.json
  def create
    @template = Template.new(template_params)
     @users = User.order(:first_name)

    respond_to do |format|
      if @template.save
        save_selected_users

        format.html { redirect_to admins_templates_path, notice: 'Template was successfully created.' }
        format.json { render :show, status: :ok, location: @template }
      else
        format.html { render :edit }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /templates/1
  # PATCH/PUT /templates/1.json
  def update
     @users = User.order(:first_name)
    respond_to do |format|
      if @template.update(template_params)
        save_selected_users

        format.html { redirect_to admins_templates_path, notice: 'Template was successfully updated.' }
        format.json { render :show, status: :ok, location: @template }
      else
        format.html { render :edit }
        format.json { render json: @template.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /templates/1
  # DELETE /templates/1.json
  def destroy
    @template.destroy
    respond_to do |format|
      format.html { redirect_to admins_templates_url, notice: 'Template was successfully deleted.' }
      format.json { head :no_content }
    end
  end

  private
    def save_selected_users
      @template.users.destroy_all
      if !params[:users].nil? && !params[:users].blank?
        params[:users].each do |user_id|
          user = User.find(user_id)
          @template.users << user
        end
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_template
      @template = Template.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def template_params
      params.require(:template).permit(:group, :message, :all_users)
    end
end