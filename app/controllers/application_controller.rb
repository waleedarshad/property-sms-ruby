class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #protect_from_forgery with: :exception
   helper :all
   include ApplicationHelper
   
   def authenticate_agent_from_token!
   	@user = User.where(authentication_token: params[:authentication_token]).first
    if @user
      sign_in @user, store: false
    else
      return render_error("Problem with Authentication Token",401, 401)
    end
  end

  def render_error(description, code,status)
    render json: {error: error_message_for(description, code)}, status: status
  end

  def error_message_for(description, code)
    {
      error_code: code,
      error_description: description
    }
  end

  def after_sign_in_path_for(resource)
    admins_dashboard_path  
  end

  def after_confirmation_path_for(resource_name, resource)
    root_path
  end
end