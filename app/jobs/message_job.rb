class MessageJob 
	
	include SuckerPunch::Job

	def perform(ids ={})
		ActiveRecord::Base.connection_pool.with_connection do
			if ids[:for_property_coordinate].present?
				SoldProperty.create_coordinate(ids[:for_property_coordinate])
			elsif ids[:for_contact_property_coordinate].present?
				Property.create_coordinate(ids[:for_contact_property_coordinate])
			else
				SoldProperty.messages_queue(ids[:for_property_sms])
			end
		end
	end

		def later(sec,for_property_sms)
			after(sec) do 
				perform(for_property_sms)
			end
		end
end