class Template < ActiveRecord::Base

	has_and_belongs_to_many :users

	validates_inclusion_of :group, :in => %w(A B C D E)
	validates :message, presence: true

	def as_json(options = {})
	{
		id: id,
		group: group,
		message: message
	}
	end
end