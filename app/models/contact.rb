class Contact < ActiveRecord::Base
	default_scope {where(:deleted => false)}
	belongs_to :user
	has_many :properties
	has_many :messages

	# validates :first_name, presence: true
	# validates :last_name, presence: true
	validates :mobile_number, presence: true

	attr_accessor :investment_property

	def as_json(options = {})
	{
		id: id,
		first_name: first_name,
		last_name: last_name,
		mobile_number: mobile_number,
		partner_first_name: partner_first_name,
		partner_last_name: partner_last_name,
		partner_mobile_number: partner_mobile_number,
		created_at: created_at
	}
	end
end
