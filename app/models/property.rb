class Property < ActiveRecord::Base

	# include ActionView::Helpers::ApplicationHelper
	belongs_to :contact
	# validates :first_name, presence: true
	# validates :last_name, presence: true
	# validates :mobile_number, presence: true
	scope :unverified_properties, -> {where("latitude is null and  longitude is null")}

	after_validation :mark_nil, :if => :address_changed? 	
	
	geocoded_by :full_address do |object,_result|
		
		street_number_valid = street_name_valid = suburb_valid = country_valid = postal_code_valid = state_valid = false
		unless _result.blank? 
			result = _result.first
			@postcode = result.postal_code
		 	if result.types[0] == "street_address" 
			 	result.data["address_components"].each do |component|
			 		component_hash =  component
		 			component_hash = component_hash.except("types")
			 		component_values = component_hash.values.map(&:downcase) 

			 		if component["types"][0]=="locality"
	 			
			 			long_word = {"mt" => "Mount","st" => "Saint","wst" => "West","est" => "East","nth" => "North","sth" => "South"}
			 			short_word = ["mt","st","wst","est","nth","sth" ]
						if object.suburb.present?			 			 
				 			s = object.suburb.squish!.split(' ').first.downcase		 			
				 			short_word.each do |sw|
				 				 
				 				if sw == s and s == "mt"
				 					object.suburb.sub! "Mt", long_word[s] if object.suburb.squish!.split(' ').first == "Mt"
				 					object.suburb.sub! "mt", long_word[s] if object.suburb.squish!.split(' ').first == "mt"
				 				elsif sw == s and s == "st"
				 					object.suburb.sub! "St", long_word[s] if object.suburb.squish!.split(' ').first == "St"
				 					object.suburb.sub! "st", long_word[s] if object.suburb.squish!.split(' ').first == "st"
				 				elsif sw == s and s == "wst"
				 					object.suburb.sub! "Wst", long_word[s] if object.suburb.squish!.split(' ').first == "Wst"
				 					object.suburb.sub! "wst", long_word[s] if object.suburb.squish!.split(' ').first == "wst"
				 				elsif sw == s and s == "est"
				 					object.suburb.sub! "Est", long_word[s] if object.suburb.squish!.split(' ').first == "Est"
				 					object.suburb.sub! "est", long_word[s] if object.suburb.squish!.split(' ').first == "est"
				 				elsif sw == s and s == "nth"
				 					object.suburb.sub! "Nth", long_word[s] if object.suburb.squish!.split(' ').first == "Nth"
				 					object.suburb.sub! "nth", long_word[s] if object.suburb.squish!.split(' ').first == "nth"
				 				elsif sw == s and s == "sth"
				 					object.suburb.sub! "Sth", long_word[s] if object.suburb.squish!.split(' ').first == "Sth"
				 					object.suburb.sub! "sth", long_word[s] if object.suburb.squish!.split(' ').first == "sth"
				 				end
				 			end	
				 		end
			 		end 

			 		if object.street.present? and component["types"][0]=="route"
				 		street_abbr = ApplicationController.helpers.street_abbrevitation
				 		street_abbr_keys = street_abbr.keys		
				 		object.street.downcase! unless object.street.downcase! == nil 
				 		l_street = object.street		
				 		street_componenet = l_street.squish!.split(' ')		
				 		street_componenet.each do |street_word|			
				 			if street_abbr_keys.include?(street_word)
				 				l_street.sub! /\b#{street_word}\b/,street_abbr[street_word]
				 				object.street = l_street.titleize				
				 			end
				 		end
			 		end	
			 		
			 		object.street = object.street.titleize if object.street.present?# titlizing word 

			 	 	if object.house_number.present? and component_values.include?(object.house_number.squish!.downcase) and component["types"][0]=="street_number" 
			 	 		 
			 	 		street_number_valid = true		 
			 	 	elsif object.street.present? and component_values.include?(object.street.squish!.downcase) and component["types"][0]=="route" 
			 	 		 
			 	 		street_name_valid = true
			 	 	elsif object.suburb.present? and component_values.include?(object.suburb.squish!.downcase) and component["types"][0]=="locality" 
			 	 		 
			 	 		suburb_valid = true
			 	 		 
			 	 	elsif object.state.present? and component_values.include?(object.state.squish!.downcase) and component["types"][0]=="administrative_area_level_1"  
			 	 		 
			 	 		state_valid = true
			 	 	elsif component_values.include?("australia") and component["types"][0]=="country"
			 	 		country_valid = true
			 	 	elsif object.postcode.present? and component_values.include?(object.postcode.squish!.downcase) and component["types"][0]=="postal_code" 
			 	 		 
			 	 		postal_code_valid = true
			 	 	end 
			 	end
			end		
				
		 	if street_number_valid and street_name_valid and suburb_valid and country_valid and state_valid 
		 	 		unless postal_code_valid
		 	 			 object.postcode = @postcode
		 	 		end
		 	 		object.latitude =  result.geometry["location"]["lat"]
		 	 		object.longitude =  result.geometry["location"]["lng"]
		 	 	else
		 	 		object.latitude =  nil
		 	 		object.longitude =  nil
		 	 end
		else
			object.latitude =  nil
			object.longitude =  nil
		end
	end

	after_validation :geocode          # auto-fetch coordinates
	after_save :verfied_address
	scope :unverified, -> {where("latitude is null and  longitude is null")}
	def address_changed? 
		return true if house_number_changed? or street_changed? or suburb_changed? or state_changed? or postcode_changed? and !new_record? 
	end

	def mark_nil
		self.update_column(:latitude,nil)
		self.update_column(:longitude,nil)
	end

	def full_address
		# house_number = self.house_number ? self.house_number : ''
		# street = self.street ? self.street : ''
		# suburb = self.suburb ? self.suburb : ''
		# state = self.state ? self.state : ''
		# postcode = self.postcode ? self.postcode : '' 

		# house_number + ' ' + street + ' ' + suburb + ' ' + state + ' ' + postcode + ' Australia'
		[house_number,street,suburb,state,postcode].to_a.compact.join(",").concat(",Australia")
	end

	# def full_address
	# 	"#{self.house_number}"+"#{self.street}"+"#{self.suburb}"+"#{self.state}"+"#{self.postcode}"
	# end

	def as_json(options = {})
    {
			id: id,
			unit_number: unit_number,
			house_number: house_number,
			street: street,
			investment_property: investment_property,
			suburb: suburb,
			state: state,
			postcode: postcode,
			sold: sold,
			created_at: created_at,
			verified: verified
		}
  end
  class << self
	  def create_coordinate(previous_maximum_id)
	  	3.times do 
	  		properties = Property.where("id >= ?",previous_maximum_id).unverified_properties
	  		Rails.logger.debug( ": ================= these are available only to create coordinates #{properties.inspect}")
	  		unless properties.blank?
	  			properties.each_with_index do |sp,index|
	  				result = index+1
	  				Rails.logger.debug( ": ================= i am reiterating to create coordinate and   #{result}")
	  				if result%5==0
	  					Rails.logger.debug( ": Going into sleep")
	  					sleep(1)
	  				end
	  				sp.save
	  			end
	  		end
	  	end
	  end
	 	handle_asynchronously :create_coordinate
	end

	def verfied_address
		if latitude and longitude 
			unless verified?
				self.update_column(:verified, true)
			end
		else
			if verified?
				self.update_column(:verified, false)	
			end
		end
	end
end
