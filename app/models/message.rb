class Message < ActiveRecord::Base
	# belongs_to :property
	belongs_to :sold_property
	belongs_to :contact

  has_many :message_properties
  belongs_to :user


end
