class Suburb < ActiveRecord::Base
	validates :state, presence: true
	validates :address, presence: true
	validates :close_range, :numericality => { :greater_than_or_equal_to => 0 },presence: true
	validates :medium_range, :numericality => { :greater_than_or_equal_to => 0 },presence: true
end
