class Intro < ActiveRecord::Base

	# has_and_belongs_to_many :users

	has_many :intros_users
	has_many :users,:through => :intros_users

	validates :message, presence: true

	def as_json(options = {})
	{
		id: id,
		message: message,
		user_id: options[:user_id]
	}
	end
end