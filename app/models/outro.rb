class Outro < ActiveRecord::Base

	# has_and_belongs_to_many :users
	has_many :outros_users
	has_many :users,:through => :outros_users

	validates :message, presence: true

	def as_json(options = {})
	{
		id: id,
		message: message,
		user_id: options[:user_id]
	}
	end
end