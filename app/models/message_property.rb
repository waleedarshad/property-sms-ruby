class MessageProperty < ActiveRecord::Base
  belongs_to :property
  belongs_to :sold_property
  belongs_to :contact


  def close_range_home?
   self.investment_property=="H" and self.range_property == 'C'
  end

  def medium_range_home?
    self.investment_property=='H' and self.range_property == 'M'
  end

  def close_range_investment?
    self.investment_property=="I" and self.range_property == 'C'
  end

  def medium_range_investment?
    self.investment_property=="I" and self.range_property == 'M'
  end

  def as_json(options = {})
    {
     :distance=>79,
     :investment_property=>investment_property,
     :range_property=>range_property,
     :property=>self.property

    }
    end
end
