class SoldProperty < ActiveRecord::Base
	
	# after_validation :mark_nil, :if => :address_changed? 
	geocoded_by :full_address# do |object,_result|
		# street_name_valid = street_name_valid = suburb_valid = country_valid = postal_code_valid = false
		# unless _result.blank?
		# 	result = _result.first
		#  	result.data["address_components"].each do |component|
		#  	 	if component.has_value?(object.street_number) and component["types"][0]=="street_number"
		#  	 		street_number_valid = true
		#  	 	elsif component.has_value?(object.street_name) and component["types"][0]=="route"
		#  	 		street_name_valid = true
		#  	 	elsif component.has_value?(object.suburb) and component["types"][0]=="locality"
		#  	 		suburb_valid = true
		#  	 	elsif component.has_value?("Australia") and component["types"][0]=="country"
		#  	 		country_valid = true
		#  	 	elsif component.has_value?(object.postcode) and component["types"][0]=="postal_code"
		#  	 		postal_code_valid = true
		#  	 	end
		#  	 end
		 	 
		#  	 if street_name_valid and street_name_valid and suburb_valid and country_valid or postal_code_valid
		#  	 		object.latitude =  result.geometry["location"]["lat"]
		#  	 		object.longitude =  result.geometry["location"]["lng"]
		#  	 	else
		#  	 		object.latitude =  nil
		#  	 		object.longitude =  nil
		#  	 end
		# else
		# 	object.latitude =  nil
		# 	object.longitude =  nil
		# end

	#end
	
	before_save :geocode
	has_many :messages

	scope :unverified_properties, -> {where("latitude is null and  longitude is null")}

	def full_address
		[street_number,street_name,suburb,state,postcode].to_a.compact.join(",").concat(",Australia")
	end

	def address_changed? 
		return true if street_name_changed? or street_number_changed? or suburb_changed? or state_changed? or postcode_changed? and !new_record? 
	end

	def self.valid_header?(file)
		require 'roo'
		xlsx = Roo::Spreadsheet.open(file)
		header = xlsx.sheet(0).row(1)
		return true if  header.include?("AddressID") and header.include?("FlatNumber") and header.include?("StreetNumber") and header.include?("StreetName") and header.include?("StreetType") and header.include?("PropertyType")  and header.include?("Suburb") and header.include?("Postcode") and header.include?("State") and header.include?("EventDate") and header.include?("EventPrice") and header.include?("EventPrice") and header.include?("EventTypeCode") and header.include?("ContractDate") and header.include?("TransactionDate") and header.include?("AreaSize") and header.include?("Bedrooms") and header.include?("Baths") and header.include?("Parking")      
	end

	def self.import(file)
		previous_maximum_id = SoldProperty.maximum('id') || 1

		xlsx = Roo::Spreadsheet.open(file).sheet(0) 
		header = xlsx.row(1)

		(2..xlsx.last_row).each_with_index do |i,index|
			row = Hash[[header, xlsx.row(i)].transpose]

			row["AddressID"] =  (row["AddressID"].is_a? Numeric) ? row["AddressID"].to_i.to_s : row["AddressID"]  
			row["State"] = (row["State"].is_a? Numeric) ? row["State"].to_i.to_s : row["State"] 
			row["PropertyType"] =  (row["PropertyType"].is_a? Numeric) ? row["PropertyType"].to_i.to_s : row["PropertyType"]  
			row["State"] = (row["State"].is_a? Numeric) ? row["State"].to_i.to_s : row["State"] 
			row["AreaSize"] =  (row["AreaSize"].is_a? Numeric) ? row["AreaSize"].to_i.to_s : row["AreaSize"]  
			row["Bedrooms"] = (row["Bedrooms"].is_a? Numeric) ? row["Bedrooms"].to_i.to_s : row["Bedrooms"] 
			row["Baths"] =  (row["Baths"].is_a? Numeric) ? row["Baths"].to_i.to_s : row["Baths"]  
			row["Parking"] = (row["Parking"].is_a? Numeric) ? row["Parking"].to_i.to_s : row["Parking"] 
			row["EventTypeCode"] =  (row["EventTypeCode"].is_a? Numeric) ? row["EventTypeCode"].to_i.to_s : row["EventTypeCode"]  
			row["StreetNumber"] = (row["StreetNumber"].is_a? Numeric) ? row["StreetNumber"].to_i.to_s : row["StreetNumber"] 
			row["Postcode"] = (row["Postcode"].is_a? Numeric) ? row["Postcode"].to_i.to_s : row["Postcode"] 
			row["FlatNumber"] = (row["FlatNumber"].is_a? Numeric) ? row["FlatNumber"].to_i.to_s : row["FlatNumber"] 
			
			if row["AddressID"] or row["State"] or row["PropertyType"] or row["State"] or row["AreaSize"] or row["Bedrooms"] or row["Baths"] or row["Parking"] or row["EventTypeCode"] or row["StreetNumber"] or row["Postcode"] or row["FlatNumber"] 

				val = index + 1
				if val%5 == 0
					Rails.logger.debug( ": ================= sleeping #{val}")
					sleep(1)
				end

				sold_property = SoldProperty.create(
					:address_id => row["AddressID"],
					:state => row["State"],
					:property_type => row["PropertyType"],
					:contract_date => row["ContractDate"],
					:transaction_date => row["TransactionDate"],
					:areasize	=> row["AreaSize"],
					:bedrooms => row["Bedrooms"],
					:baths => row["Baths"],
					:parking => row["Parking"],
					:eventtype_code => row["EventTypeCode"],
					:event_price => row["EventPrice"],
					:event_date => row["EventDate"],
					:street_type => row["StreetType"],
					:street_name => row["StreetName"],
					:street_number => row["StreetNumber"],
					:suburb => row["Suburb"],
					:postcode => row["Postcode"],
					:flat_number => row["FlatNumber"])
			end
		end
		previous_maximum_id
	end

	class << self
		def messages_queue(previous_maximum_id)

			sold_properties = SoldProperty.where("id > ?",previous_maximum_id)
			previous_maximum_message_id = Message.maximum(:id) || 1

			sold_properties.each do |sp|
				properties = Property.near([sp.latitude, sp.longitude], 0.186411).where("contact_id is not null").includes(:contact).limit(10)		
				properties.each do |p|
					if p.contact.present?
						if p.latitude == sp.latitude and p.longitude == sp.longitude 
							unless p.unit_number == sp.flat_number
								create_message(p,sp)
							end 
						else
							create_message(p,sp)
						end 
					end # ending p.contact.property.present?
			  end	# property loop ending 
		end# sold property ending
			# For IOS Sending notifications
			certificate = File.read("config/production_ck.pem")
			connection = Houston::Connection.new(Houston::APPLE_PRODUCTION_GATEWAY_URI, certificate,"123456")
			connection.open
			messages = Message.where("id > ? && user_id is not null" ,previous_maximum_message_id).group("user_id").includes(:user)

			messages.each do |m|
				u = m.user
				if u.present?
					unless u.ud_ID == nil or u.ud_ID == "(null)"
						u.send_notification(connection)				
					end
					unless u.udId_android == nil or u.udId_android == "(null)"
						u.send_notification_android(u.udId_android)
					end
				end	
			end

			connection.close

		end

		handle_asynchronously :messages_queue, :run_at => Proc.new { 3.minutes.from_now }

		def create_coordinate(previous_maximum_id)
			3.times do 
				soldproperties = SoldProperty.where("id >= ?",previous_maximum_id).unverified_properties
				Rails.logger.debug( ": =====Enter into Create Coordinate if any remaing #{soldproperties.inspect}")
				unless soldproperties.blank?
					soldproperties.each_with_index do |sp,index|
						result = index+1
						Rails.logger.debug( ": ================= i am reiterating to create coordinate and counter #{result}")
						if result%5==0
							Rails.logger.debug( ": ================= sleeping #{result}")
							sleep(1)
						end
						sp.save
					end
				end
			end
		end
		handle_asynchronously :create_coordinate


		def create_message(p,sp)
			suburb_range = Suburb.where("lower(address) = ? && lower(state) = ?", sp.suburb.downcase,sp.state.downcase).first				
			if suburb_range and suburb_range.close_range.present? and suburb_range.medium_range.present?
				suburb_close_range = suburb_range.close_range * 0.00062137 #converting to miles
				suburb_medium_range = suburb_range.medium_range * 0.00062137 #converting to miles
			end

			suburb_close_range = suburb_close_range ? suburb_close_range : 0.0497097#80
			suburb_medium_range = suburb_medium_range ? suburb_medium_range : 0.186411 # 300 meter default
			
			message = Message.where(:contact_id => p.contact_id,sold_property_id: sp.id).first
			
			if p.distance >= 0 and p.distance <= suburb_medium_range
				unless message
					message = Message.create(contact_id: p.contact_id,sold_property_id: sp.id,user_id: p.contact.user_id)
				end
				message.message_properties.create(
					property_id: p.id,
					distance: p.distance / 0.00062137,# converting to meter 
					investment_property: (p.investment_property) ? 'I' : 'H',
					range_property: (p.distance <= suburb_close_range) ? 'C' : 'M'  #0.0497097 == 40 meter default)
				)
			end

			message_properties_count = message ? message.message_properties.count : 0
			if message and  message_properties_count>=2
				 message.update_column(:template, 'E')
			elsif message and message_properties_count == 1
				msg_property = message.message_properties.first
				if msg_property
					if msg_property.close_range_home?
						message.update_column(:template, 'B')
					elsif msg_property.medium_range_home?
						message.update_column(:template, 'A')
					elsif msg_property.close_range_investment?
						message.update_column(:template, 'D')
					elsif msg_property.medium_range_investment?
						message.update_column(:template, 'C')
					end
				end	
			end # end msg_property_count
		end

	end

	def self.messages_expire
		messages = Message.where('date(created_at) < ? AND sent = false', Date.today - 10)

		messages.each do |m|
			m.delete
		end
  end
  	
end