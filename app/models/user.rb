class User < ActiveRecord::Base
   extend Enumerize

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_secure_token :authentication_token
  has_many :contacts
  # has_and_belongs_to_many :intros
  has_many :intros_users, -> { where admin: true }
  has_many :intros, :through => :intros_users

  has_many :intros_users_user, -> { where admin: false }, class_name: 'IntrosUser'
  has_many :intros_admin_false, :through => :intros_users_user,:source => :intro #, class_name: 'Intro'


  has_many :outros_users, -> { where admin: true }
  has_many :outros, :through => :outros_users

  has_many :outros_users_user, -> { where admin: false }, class_name: 'OutrosUser'
  has_many :outros_admin_false, :through => :outros_users_user,:source => :outro #, class_name: 'Intro'


  enumerize :subscription, in: {
    "0-40 Contacts" => 40,
    "au.com.propertysmsapp.41_81" => 81, 
    "au.com.propertysmsapp.81_120" => 120,
    "au.com.propertysmsapp.121_160" => 160,
    "au.com.propertysmsapp.161_200" => 200,
    "au.com.propertysmsapp.201_500" => 500,
    "au.com.propertysmsapp.501" => 501}

  # has_many :outros_users
  # has_many :outros,:through => :outros_users

  # has_and_belongs_to_many :outros
  has_and_belongs_to_many :templates

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :mobile_number, presence: true

  def full_name
     "#{first_name} #{last_name}"
  end

  def as_json(options = {})
    if options[:subscription]
      {
        email: email,
        subscription: subscription,
        subscription_expired_at: subscription_expired_at,
        contacts_count: self.contacts.count
      }
    else
      {
        id: id,
        email: email,
        first_name: first_name,
        last_name: last_name,
        company: company,
        mobile_number: mobile_number,
        authentication_token: authentication_token,
        created_at: created_at,
        contacts_count: self.contacts.count,
        terms_accepted: terms_accepted,
        subscription: subscription,
        subscription_expired_at: subscription_expired_at
      }
    end
  end

  def self.import_contacts(params)

    user = User.find(params[:id])
    user_contact = user.contacts
    contact_hash = []
    
    xlsx = Roo::Spreadsheet.open(params[:file].path).sheet(0) 
    header = xlsx.row(1)

    (2..xlsx.last_row).each do |i|
      row = Hash[[header, xlsx.row(i)].transpose]
      contact_hash << row
    end
    contact_hash
    # CSV.foreach(params[:file].path, headers: true) do |row|
    #   unless row.to_hash.blank?
    #     contact_hash << row.to_hash
    #   end
    # end
    # contact_hash
  end

  def self.import(params)
    user = User.find(params[:id])
    previous_maximum_id = Property.maximum('id') || 1
    user_contact = Contact.unscoped.where(:user_id => user.id)
    contact_ids =  params[:contact_ids].map(&:to_i)
    
    xlsx = Roo::Spreadsheet.open(params[:file]).sheet(0)
    header = xlsx.row(1)
    
    sleep_count = 0

    (2..xlsx.last_row).each do |i|
      row = Hash[[header, xlsx.row(i)].transpose]

      sleep_count = sleep_count+1 if sleep_count == 0

      if contact_ids.include?(i)
        contact_hash = row.to_hash

        contact_hash["Mobile Number"] =  (contact_hash["Mobile Number"].is_a? Numeric) ? contact_hash["Mobile Number"].to_i.to_s : contact_hash["Mobile Number"]  
        contact_hash["Unit Number1"] = (contact_hash["Unit Number1"].is_a? Numeric) ? contact_hash["Unit Number1"].to_i.to_s : contact_hash["Unit Number1"] 
        contact_hash["House Number1"] = (contact_hash["House Number1"].is_a? Numeric) ? contact_hash["House Number1"].to_i.to_s : contact_hash["House Number1"] 
        contact_hash["Street1"] = (contact_hash["Street1"].is_a? Numeric) ? contact_hash["Street1"].to_i.to_s : contact_hash["Street1"] 
        contact_hash["State1"] = (contact_hash["State1"].is_a? Numeric) ? contact_hash["State1"].to_i.to_s :  contact_hash["State1"] 
        contact_hash["Post Code1"] = (contact_hash["Post Code1"].is_a? Numeric) ?  contact_hash["Post Code1"].to_i.to_s : contact_hash["Post Code1"]
        contact_hash["Unit Number2"] = (contact_hash["Unit Number2"].is_a? Numeric) ? contact_hash["Unit Number2"].to_i.to_s : contact_hash["Unit Number2"] 
        contact_hash["House Number2"] = (contact_hash["House Number2"].is_a? Numeric) ? contact_hash["House Number2"].to_i.to_s : contact_hash["House Number2"] 
        contact_hash["Street2"] = (contact_hash["Street2"].is_a? Numeric) ? contact_hash["Street2"] : contact_hash["Street2"]
        contact_hash["State2"] = (contact_hash["State2"].is_a? Numeric) ? contact_hash["State2"].to_i.to_s : contact_hash["State2"]
        contact_hash["Post Code2"] = (contact_hash["Post Code2"].is_a? Numeric) ? contact_hash["Post Code2"].to_i.to_s : contact_hash["Post Code2"]
        contact_hash["Unit Number3"] = (contact_hash["Unit Number3"].is_a? Numeric) ? contact_hash["Unit Number3"].to_i.to_s : contact_hash["Unit Number3"] 
        contact_hash["House Number3"] = (contact_hash["House Number3"].is_a? Numeric) ? contact_hash["House Number3"].to_i.to_s : contact_hash["House Number3"] 
        contact_hash["Street3"] = (contact_hash["Street3"].is_a? Numeric) ?  contact_hash["Street3"].to_i.to_s :  contact_hash["Street3"] 
        contact_hash["State3"] = (contact_hash["State3"].is_a? Numeric) ? contact_hash["State3"].to_i.to_s : contact_hash["State3"] 
        contact_hash["Post Code3"] = (contact_hash["Post Code3"].is_a? Numeric) ? contact_hash["Post Code3"].to_i.to_s : contact_hash["Post Code3"]

        if contact_hash["Investment Property1"] == "TRUE" or contact_hash["Investment Property1"] == "true" or contact_hash["Investment Property1"] == 1.0 or contact_hash["Investment Property1"] == 1 or contact_hash["Investment Property1"] == true
          contact_hash["Investment Property1"] = true
        end

        if contact_hash["Investment Property1"] == "FALSE" or contact_hash["Investment Property1"] == "false" or contact_hash["Investment Property1"] == 0.0 or contact_hash["Investment Property1"] == 0 or contact_hash["Investment Property1"] == false
          contact_hash["Investment Property1"] = false
        end

        if contact_hash["Investment Property2"] == "TRUE" or contact_hash["Investment Property2"] == "true" or contact_hash["Investment Property2"] == 1.0 or contact_hash["Investment Property2"] == 1 or contact_hash["Investment Property2"] == true
          contact_hash["Investment Property2"] = true
        end

        if contact_hash["Investment Property2"] == "FALSE" or contact_hash["Investment Property2"] == "false" or contact_hash["Investment Property2"] == 0.0 or contact_hash["Investment Property2"] == 0 or contact_hash["Investment Property2"] == false
          contact_hash["Investment Property2"] = false
        end

        if contact_hash["Investment Property3"] == "TRUE" or contact_hash["Investment Property3"] == "true" or contact_hash["Investment Property3"] == 1.0 or contact_hash["Investment Property3"] == 1 or contact_hash["Investment Property3"] == true
          contact_hash["Investment Property3"] = true
        end

        if contact_hash["Investment Property3"] == "FALSE" or contact_hash["Investment Property3"] == "false" or contact_hash["Investment Property3"] == 0.0 or contact_hash["Investment Property3"] == 0 or contact_hash["Investment Property3"] == false
          contact_hash["Investment Property3"] = false
        end


        contact = user_contact.where(:mobile_number => contact_hash["Mobile Number"]).first

        if contact and contact.deleted?
          contact.update_column(:deleted,false)
        # else
        #   contact = user_contact.where(:mobile_number => contact_hash["mobile_number"]).first
        end
        if contact.blank? 
          contact = user.contacts.build(:first_name => contact_hash["First Name"] , :last_name => contact_hash["Last Name"],:mobile_number => contact_hash["Mobile Number"],:partner_first_name => contact_hash["Partner First Name"],:partner_last_name => contact_hash["Partner Last Name"])
          contact_true = contact.save
        end
        if contact || contact_true 
          
          if contact_hash["Unit Number1"].present? or  contact_hash["House Number1"].present? or contact_hash["Street1"].present? or contact_hash["Suburb1"].present? or contact_hash["State1"].present? or contact_hash["Post Code1"].present?
            p = contact.properties.find_or_initialize_by(:unit_number => contact_hash["Unit Number1"],:house_number => contact_hash["House Number1"],
              :street=> contact_hash["Street1"],
              :suburb=> contact_hash["Suburb1"],:state => contact_hash["State1"],:postcode => contact_hash["Post Code1"])
            if p.new_record?
              p.investment_property = contact_hash["Investment Property1"]
              sleep_count +=1
            end
            p.save
          end

          if contact_hash["Unit Number2"].present? or  contact_hash["House Number2"].present? or contact_hash["Street2"].present? or contact_hash["Suburb2"].present? or contact_hash["State2"].present? or contact_hash["Post Code2"].present?

            p = contact.properties.find_or_initialize_by(:unit_number => contact_hash["Unit Number2"],:house_number => contact_hash["House Number2"],
              :street=> contact_hash["Street2"],
              :suburb=> contact_hash["Suburb2"],:state => contact_hash["State2"],:postcode => contact_hash["Post Code2"])
            if p.new_record?
              p.investment_property = contact_hash["Investment Property2"]
              sleep_count +=1
            end
            p.save
          end

          if contact_hash["Unit Number3"].present? or  contact_hash["House Number3"].present? or contact_hash["Street3"].present? or contact_hash["Suburb3"].present? or contact_hash["State3"].present? or contact_hash["Post Code3"].present?
            p = contact.properties.find_or_initialize_by(:unit_number => contact_hash["Unit Number3"],:house_number => contact_hash["House Number3"],
              :street=> contact_hash["Street3"],
              :suburb=> contact_hash["Suburb3"],:state => contact_hash["State3"],:postcode => contact_hash["Post Code3"])
            if p.new_record?
              p.investment_property = contact_hash["Investment Property3"]
              sleep_count +=1
            end
            p.save
          end
        end
      end
      puts "I am checking sleep_count #{sleep_count}================================================================"
      if sleep_count%5 == 0 
        sleep_count = 0
        puts "Going into sleep= ================================================================"
        sleep(1)
        puts "Getting up from sleep= ================================================================"
      end
    end
    previous_maximum_id
    
  end

  def send_notification(connection)

    # certificate = File.read("config/production_ck.pem")

    # connection = Houston::Connection.new(Houston::APPLE_PRODUCTION_GATEWAY_URI, certificate,"123456")
    # connection.open
    # puts "connection open and ready for messages for send notification "

    token = self.ud_ID
    Rails.logger.debug("#{self.ud_ID}")
    if token

      ids = self.contacts.pluck(:id)
      total_count =  Message.where(:contact_id => ids,:sent=>false).count

      notification = Houston::Notification.new(device: token)
      notification.alert = "You have messages ready to send."
      notification.badge = total_count
      notification.sound = "sosumi.aiff"
      notification.category = "INVITE_CATEGORY"
      notification.content_available = true
      notification.custom_data = {foo: "bar"}
      connection.write(notification.message)

      Rails.logger.debug( ": ================= notification sent ")
    end
  end

  def send_notification_android(token)
    Push::MessageGcm.create(
        app: 'PropertySMS',
        device: token,
        payload: { message: 'You have messages ready to send.' },
        collapse_key: 'MSG')
  end


  def self.delete_message_notification
    puts "--------------- running task -------------------"
    Rails.logger.debug("--------------- running task -------------------")
    certificate = File.read("config/production_ck.pem")

    connection = Houston::Connection.new(Houston::APPLE_PRODUCTION_GATEWAY_URI, certificate,"123456")
    connection.open
    # Rails.logger.debug("connection opsen and ready for messages for deleted messages notification ")

    User.all.each do |u|
      token = u.ud_ID
      android_token = u.udId_android
      if token
        ids = u.contacts.pluck(:id)
        total_messages =  Message.where(:contact_id => ids,:sent=>false)
      
        if total_messages.where("date(created_at) =?", Date.today - 7).present?
          m = "You have messages that are expiring in 3 days."
          batch = total_messages.where("date(created_at) =?", Date.today - 7).count
          send_expire_message_notification(m,connection,token,batch)
          send_expire_message_notification_android(android_token,m) if android_token.present?
           sleep(1)
        end

        if total_messages.where("date(created_at) =?", Date.today - 8).present?
          batch = total_messages.where("date(created_at) =?", Date.today - 8).count
          m = "You have messages that are expiring in 2 days."
          send_expire_message_notification(m,connection,token,batch)
          send_expire_message_notification_android(android_token,m) if android_token.present?
          sleep(1)
        end

        if total_messages.where("date(created_at) =?", Date.today - 9).present?
          batch = total_messages.where("date(created_at) =?", Date.today - 9).count
          m = "You have messages that are expiring in 1 day."
          send_expire_message_notification(m,connection,token,batch)
          send_expire_message_notification_android(android_token,m) if android_token.present?
          sleep(1)
        end

        if total_messages.where("date(created_at) =?", Date.today - 10).present?
          batch = total_messages.where("date(created_at) =?", Date.today - 10).count
          m = "You have messages that are expiring in today."
          send_expire_message_notification(m,connection,token,batch)
          send_expire_message_notification_android(android_token,m) if android_token.present?
        end
      end
    end
    connection.close
  end

  def self.send_expire_message_notification_android(token,m)
    Push::MessageGcm.create(
        app: 'PropertySMS',
        device: token,
        payload: { message: m },
        collapse_key: 'MSG')
  end

  def self.send_expire_message_notification(message,connection,token,batch)
    Rails.logger.debug("notification is  ready  for deleted messages notification ")
    notification = Houston::Notification.new(device: token)
    notification.alert = message
    notification.badge = batch
    notification.sound = "sosumi.aiff"
    notification.category = "INVITE_CATEGORY"
    notification.content_available = true
    # notification.custom_data = {foo: "bar"}
    connection.write(notification.message)
    Rails.logger.debug("notification is  reaching  for deleted messages notification ")
  end

  def self.valid_header?(file)
    require 'roo'
    xlsx = Roo::Spreadsheet.open(file)
    header = xlsx.sheet(0).row(1)
    return true if  header.include?("First Name") and header.include?("Last Name") and header.include?("Mobile Number") and header.include?("Partner First Name") and header.include?("Partner Last Name") and header.include?("Unit Number1") and header.include?("House Number1") and header.include?("Street1") and header.include?("Suburb1") and header.include?("State1") and header.include?("Post Code1")and header.include?("Unit Number2") and header.include?("House Number2") and header.include?("Street2") and header.include?("Suburb2") and header.include?("State2") and header.include?("Post Code2") and header.include?("Unit Number3") and header.include?("House Number3") and header.include?("Street3") and header.include?("Suburb3") and header.include?("State3") and header.include?("Post Code3")
  end


  # def self.valid_header?(params)
  #   header = CSV.open(params[:file].path, 'r') { |csv| csv.first } 
  #   return true if  header.include?("first_name") and header.include?("last_name") and header.include?("mobile_number") and header.include?("unit_number") and header.include?("house_number")   
  # end

end