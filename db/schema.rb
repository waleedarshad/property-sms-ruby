# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150909104012) do

  create_table "admin_intros", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "intro_id",   limit: 4
    t.boolean  "admin",      limit: 1, default: true, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "admin_outros", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "outro_id",   limit: 4
    t.boolean  "admin",      limit: 1, default: true, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "status",                 limit: 4,   default: 0,  null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "contacts", force: :cascade do |t|
    t.string   "first_name",            limit: 255
    t.string   "last_name",             limit: 255
    t.string   "mobile_number",         limit: 255
    t.string   "partner_first_name",    limit: 255
    t.string   "partner_last_name",     limit: 255
    t.string   "partner_mobile_number", limit: 255
    t.integer  "user_id",               limit: 4
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.boolean  "deleted",               limit: 1,   default: false, null: false
  end

  add_index "contacts", ["deleted"], name: "index_contacts_on_deleted", using: :btree
  add_index "contacts", ["first_name"], name: "index_contacts_on_first_name", using: :btree
  add_index "contacts", ["id", "deleted"], name: "index_contacts_on_id_and_deleted", using: :btree
  add_index "contacts", ["last_name"], name: "index_contacts_on_last_name", using: :btree
  add_index "contacts", ["mobile_number"], name: "index_contacts_on_mobile_number", using: :btree
  add_index "contacts", ["user_id", "mobile_number"], name: "index_contacts_on_user_id_and_mobile_number", using: :btree
  add_index "contacts", ["user_id"], name: "index_contacts_on_user_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   limit: 4,     default: 0, null: false
    t.integer  "attempts",   limit: 4,     default: 0, null: false
    t.text     "handler",    limit: 65535,             null: false
    t.text     "last_error", limit: 65535
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by",  limit: 255
    t.string   "queue",      limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "intros", force: :cascade do |t|
    t.text     "message",    limit: 65535
    t.boolean  "all_users",  limit: 1,     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "intros", ["message"], name: "index_intros_on_message", length: {"message"=>25}, using: :btree

  create_table "intros_users", force: :cascade do |t|
    t.integer "intro_id", limit: 4
    t.integer "user_id",  limit: 4
    t.boolean "admin",    limit: 1, default: true, null: false
  end

  add_index "intros_users", ["intro_id", "user_id"], name: "index_intros_users_on_intro_id_and_user_id", using: :btree
  add_index "intros_users", ["user_id"], name: "index_intros_users_on_user_id", using: :btree

  create_table "message_properties", force: :cascade do |t|
    t.integer  "message_id",          limit: 4
    t.integer  "property_id",         limit: 4
    t.decimal  "distance",                        precision: 10
    t.string   "investment_property", limit: 255
    t.string   "range_property",      limit: 255
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
  end

  add_index "message_properties", ["message_id"], name: "index_message_properties_on_message_id", using: :btree
  add_index "message_properties", ["property_id"], name: "index_message_properties_on_property_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "contact_id",       limit: 4
    t.text     "message_body",     limit: 65535
    t.boolean  "sent",             limit: 1,     default: false, null: false
    t.date     "sent_date_at"
    t.boolean  "expired",          limit: 1,     default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sold_property_id", limit: 4
    t.string   "template",         limit: 255
    t.integer  "user_id",          limit: 4
  end

  add_index "messages", ["contact_id", "sent"], name: "index_messages_on_contact_id_and_sent", using: :btree
  add_index "messages", ["contact_id", "sold_property_id"], name: "index_messages_on_contact_id_and_sold_property_id", using: :btree
  add_index "messages", ["contact_id"], name: "index_messages_on_contact_id", using: :btree
  add_index "messages", ["sent"], name: "index_messages_on_sent", using: :btree
  add_index "messages", ["sold_property_id"], name: "index_messages_on_sold_property_id", using: :btree

  create_table "outros", force: :cascade do |t|
    t.text     "message",    limit: 65535
    t.boolean  "all_users",  limit: 1,     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "outros", ["message"], name: "index_outros_on_message", length: {"message"=>25}, using: :btree

  create_table "outros_users", force: :cascade do |t|
    t.integer "outro_id", limit: 4
    t.integer "user_id",  limit: 4
    t.boolean "admin",    limit: 1, default: true, null: false
  end

  add_index "outros_users", ["outro_id", "user_id"], name: "index_outros_users_on_outro_id_and_user_id", using: :btree
  add_index "outros_users", ["user_id"], name: "index_outros_users_on_user_id", using: :btree

  create_table "properties", force: :cascade do |t|
    t.integer  "contact_id",          limit: 4
    t.string   "unit_number",         limit: 255
    t.string   "house_number",        limit: 255
    t.string   "street",              limit: 255
    t.boolean  "investment_property", limit: 1,                             default: false
    t.string   "suburb",              limit: 255
    t.string   "state",               limit: 255
    t.string   "postcode",            limit: 255
    t.decimal  "latitude",                        precision: 20, scale: 15
    t.decimal  "longitude",                       precision: 20, scale: 15
    t.datetime "created_at",                                                                null: false
    t.datetime "updated_at",                                                                null: false
    t.boolean  "sold",                limit: 1,                             default: false, null: false
    t.boolean  "verified",            limit: 1,                             default: false, null: false
  end

  add_index "properties", ["contact_id"], name: "index_properties_on_contact_id", using: :btree
  add_index "properties", ["latitude", "longitude"], name: "index_properties_on_latitude_and_longitude", using: :btree
  add_index "properties", ["latitude"], name: "index_properties_on_latitude", using: :btree
  add_index "properties", ["longitude"], name: "index_properties_on_longitude", using: :btree

  create_table "push_configurations", force: :cascade do |t|
    t.string   "type",        limit: 255,                   null: false
    t.string   "app",         limit: 255,                   null: false
    t.text     "properties",  limit: 65535
    t.boolean  "enabled",     limit: 1,     default: false, null: false
    t.integer  "connections", limit: 4,     default: 1,     null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "push_feedback", force: :cascade do |t|
    t.string   "app",          limit: 255,                   null: false
    t.string   "device",       limit: 255,                   null: false
    t.string   "type",         limit: 255,                   null: false
    t.string   "follow_up",    limit: 255,                   null: false
    t.datetime "failed_at",                                  null: false
    t.boolean  "processed",    limit: 1,     default: false, null: false
    t.datetime "processed_at"
    t.text     "properties",   limit: 65535
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "push_feedback", ["processed"], name: "index_push_feedback_on_processed", using: :btree

  create_table "push_messages", force: :cascade do |t|
    t.string   "app",               limit: 255,                   null: false
    t.string   "device",            limit: 255,                   null: false
    t.string   "type",              limit: 255,                   null: false
    t.text     "properties",        limit: 65535
    t.boolean  "delivered",         limit: 1,     default: false, null: false
    t.datetime "delivered_at"
    t.boolean  "failed",            limit: 1,     default: false, null: false
    t.datetime "failed_at"
    t.integer  "error_code",        limit: 4
    t.string   "error_description", limit: 255
    t.datetime "deliver_after"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "push_messages", ["delivered", "failed", "deliver_after"], name: "index_push_messages_on_delivered_and_failed_and_deliver_after", using: :btree

  create_table "sold_properties", force: :cascade do |t|
    t.string   "address_id",       limit: 255
    t.string   "postcode",         limit: 255
    t.decimal  "latitude",                     precision: 20, scale: 15
    t.decimal  "longitude",                    precision: 20, scale: 15
    t.string   "state",            limit: 255
    t.string   "property_type",    limit: 255
    t.string   "contract_date",    limit: 255
    t.string   "transaction_date", limit: 255
    t.string   "areasize",         limit: 255
    t.integer  "bedrooms",         limit: 4
    t.integer  "baths",            limit: 4
    t.integer  "parking",          limit: 4
    t.string   "eventtype_code",   limit: 255
    t.string   "event_price",      limit: 255
    t.string   "event_date",       limit: 255
    t.string   "suburb",           limit: 255
    t.string   "street_type",      limit: 255
    t.string   "street_name",      limit: 255
    t.string   "street_number",    limit: 255
    t.string   "flat_number",      limit: 255
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
  end

  add_index "sold_properties", ["latitude", "longitude"], name: "index_sold_properties_on_latitude_and_longitude", using: :btree
  add_index "sold_properties", ["latitude"], name: "index_sold_properties_on_latitude", using: :btree
  add_index "sold_properties", ["longitude"], name: "index_sold_properties_on_longitude", using: :btree

  create_table "suburbs", force: :cascade do |t|
    t.string   "state",        limit: 255
    t.string   "address",      limit: 255
    t.integer  "close_range",  limit: 4
    t.integer  "medium_range", limit: 4
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "suburbs", ["address", "state"], name: "index_suburbs_on_address_and_state", using: :btree
  add_index "suburbs", ["address"], name: "index_suburbs_on_address", using: :btree
  add_index "suburbs", ["state"], name: "index_suburbs_on_state", using: :btree

  create_table "templates", force: :cascade do |t|
    t.string   "group",      limit: 255
    t.text     "message",    limit: 65535
    t.boolean  "all_users",  limit: 1,     default: true, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "templates", ["group"], name: "index_templates_on_group", using: :btree
  add_index "templates", ["message"], name: "index_templates_on_message", length: {"message"=>25}, using: :btree

  create_table "templates_users", id: false, force: :cascade do |t|
    t.integer "template_id", limit: 4
    t.integer "user_id",     limit: 4
  end

  add_index "templates_users", ["template_id", "user_id"], name: "index_templates_users_on_template_id_and_user_id", using: :btree
  add_index "templates_users", ["user_id"], name: "index_templates_users_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                   limit: 255, default: "",                null: false
    t.string   "encrypted_password",      limit: 255, default: ""
    t.string   "reset_password_token",    limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",           limit: 4,   default: 0,                 null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",      limit: 255
    t.string   "last_sign_in_ip",         limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                    limit: 255
    t.string   "confirmation_token",      limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",       limit: 255
    t.string   "invitation_token",        limit: 255
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit",        limit: 4
    t.integer  "invited_by_id",           limit: 4
    t.string   "invited_by_type",         limit: 255
    t.integer  "invitations_count",       limit: 4,   default: 0
    t.string   "first_name",              limit: 255
    t.string   "last_name",               limit: 255
    t.string   "company",                 limit: 255
    t.string   "mobile_number",           limit: 255
    t.string   "authentication_token",    limit: 255
    t.string   "customer_id",             limit: 255
    t.string   "ud_ID",                   limit: 255
    t.boolean  "terms_accepted",          limit: 1,   default: false,             null: false
    t.date     "subscription_expired_at"
    t.string   "udId_android",            limit: 255
    t.string   "device",                  limit: 255
    t.string   "subscription",            limit: 255, default: "0 - 40 contacts"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["first_name"], name: "index_users_on_first_name", using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
