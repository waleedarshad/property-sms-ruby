class ChangingDefaultToTrue < ActiveRecord::Migration
  def change
  	change_column :intros, :all_users, :boolean, :default => true, :null => false
  	change_column :outros, :all_users, :boolean, :default => true, :null => false
  	change_column :templates, :all_users, :boolean, :default => true, :null => false
  end
end
