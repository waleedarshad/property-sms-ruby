class AddTemplateFieldToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :template, :string
    remove_column :messages, :property_id,:integer
    remove_column :messages, :distance,:decimal
    remove_column :messages, :investment_property,:integer
    remove_column :messages, :range_property,:integer
    remove_column :message_properties, :sold_property_id,:integer
  end
end
