class AddTermsAndConditionsFieldToUsers < ActiveRecord::Migration
  def change
    add_column :users, :terms_accepted, :boolean,:default => false,null: false
  end
end
