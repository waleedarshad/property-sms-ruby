class AddIndexesToDatabaseFields < ActiveRecord::Migration
  def change

    # add index on users tables;
    add_index :users, :authentication_token,unique: true
    add_index :users, :first_name

    # # add index on 
    add_index :contacts, :first_name
    add_index :contacts, :last_name
    add_index :contacts, :mobile_number
    add_index :contacts, :user_id
    add_index :contacts, :deleted
    
    # # add indexes on properties
    add_index :properties, :contact_id
    add_index :properties, :latitude
    add_index :properties, :longitude
    add_index :properties, [:latitude,:longitude]
    # add_index :properties, [:unit_number,:house_number,:street,:suburb,:state,:postcode], :name => 'index'
    # add_index :properties, :unit_number
    # add_index :properties, :house_number
    # add_index :properties, :street
    # add_index :properties, :subscription
    # add_index :properties, :type
    # add_index :properties, :suburb
    # add_index :properties, :postcode
    # add_index :properties, :investment_property
    # add_index :properties, :verified
    # add_index :properties, :sold

    # add indexes on intro,outros,templates
    add_index :intros, :message,:length => { :message => 25 }
    add_index :outros, :message,:length => { :message => 25 }
    add_index :templates, :message,:length => { :message => 25 }
    add_index :templates, :group

    # add indexes on sold properties
    add_index :sold_properties, :latitude
    add_index :sold_properties, :longitude
    add_index :sold_properties, [:latitude,:longitude]
    # add_index :sold_properties, :postcode
    # add_index :sold_properties, :state
    # add_index :sold_properties, :property_type
    # add_index :sold_properties, :contract_date
    # add_index :sold_properties, :transaction_date
    # add_index :sold_properties, :areasize
    # add_index :sold_properties, :bedrooms
    # add_index :sold_properties, :baths
    # add_index :sold_properties, :parking
    # add_index :sold_properties, :eventtype_code
    # add_index :sold_properties, :event_price
    # add_index :sold_properties, :event_date
    # add_index :sold_properties, :suburb
    # add_index :sold_properties, :street_type
    # add_index :sold_properties, :street_name
    # add_index :sold_properties, :street_number
    # add_index :sold_properties, :flat_number
    # add_index :sold_properties, :sold

    

    # add indexes on messages

    add_index :messages, :contact_id
    add_index :messages, :sold_property_id
    add_index :messages, :sent

    # add_index :messages, :sent_date_at
    # add_index :messages, :expired
    # add_index :messages, :distance
    # add_index :messages, :investment_property
    # add_index :messages, :range_property
    # add_index :messages, :deleted


    # add indexes on message_properties
    add_index :message_properties, :message_id
    add_index :message_properties, :property_id


    # add indexes to suburb
    add_index :suburbs,:address
    add_index :suburbs,:state
  end
end