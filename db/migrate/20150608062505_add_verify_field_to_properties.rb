class AddVerifyFieldToProperties < ActiveRecord::Migration
  def change
    add_column :properties, :verified, :boolean ,:default => false,null: false
  end
end
