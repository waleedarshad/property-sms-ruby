class AddLatitudeAndLongitudeToModel < ActiveRecord::Migration
  def change
    add_column :properties, :latitude, :float, :after => :postcode
    add_column :properties, :longitude, :float, :after => :latitude

    add_column :sold_properties, :latitude, :float, :after => :postcode
    add_column :sold_properties, :longitude, :float, :after => :latitude
  end
end
