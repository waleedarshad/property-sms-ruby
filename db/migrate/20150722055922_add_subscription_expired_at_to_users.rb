class AddSubscriptionExpiredAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :subscription_expired_at, :date
  end
end
