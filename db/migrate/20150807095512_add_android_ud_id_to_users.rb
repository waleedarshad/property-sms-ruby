class AddAndroidUdIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :udId_android, :string
    add_column :users, :device, :string
  end
end
