class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :first_name
      t.string :last_name
      t.string :mobile_number
      t.string :unit_number
      t.string :house_number
      t.string :street
      t.string :type
      t.string :suburb
      t.string :state
      t.string :postcode
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
