class AddSoldFieldProperty < ActiveRecord::Migration
  def change
  	add_column :properties ,:sold ,:boolean ,null: false,:default => false
  end
end
