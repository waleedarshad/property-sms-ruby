class MessageFieldInMessages < ActiveRecord::Migration
  def change
    add_column :messages, :message_body, :string, :after => :sold_property_id
  end
end
