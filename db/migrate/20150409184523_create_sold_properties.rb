class CreateSoldProperties < ActiveRecord::Migration
  def change
    create_table :sold_properties do |t|
      t.string :address_id
      t.string :postcode
      t.string :state
      t.string :property_type
      t.string :contract_date
      t.string :transaction_date
      t.string :areasize
      t.integer :bedrooms
      t.integer :baths
      t.integer :parking
      t.string :eventtype_code
      t.string :event_price
      t.string :event_date
      t.string :suburb
      t.string :street_type
      t.string :street_name
      t.string :street_number
      t.string :flat_number

      t.timestamps null: false
    end
  end
end
