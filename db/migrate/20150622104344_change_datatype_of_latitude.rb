class ChangeDatatypeOfLatitude < ActiveRecord::Migration
  def up
  	 change_column :properties, :latitude,  :decimal, :precision => 20,:scale => 15
  	 change_column :properties, :longitude, :decimal, :precision => 20,:scale => 15

  	 change_column :sold_properties, :latitude,  :decimal, :precision => 20,:scale => 15
  	 change_column :sold_properties, :longitude, :decimal, :precision => 20,:scale => 15
  end

  def down
  	change_column :properties, :latitude,  :float
  	change_column :properties, :longitude, :float

  	change_column :sold_properties, :latitude,  :float
  	change_column :sold_properties, :longitude, :float
  end
end
