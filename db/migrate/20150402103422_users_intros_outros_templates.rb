class UsersIntrosOutrosTemplates < ActiveRecord::Migration
  def change
    create_table :intros_users, :id => false do |t|
        t.references :intro
        t.references :user
    end
    add_index :intros_users, [:intro_id, :user_id]
    add_index :intros_users, :user_id

    create_table :outros_users, :id => false do |t|
        t.references :outro
        t.references :user
    end
    add_index :outros_users, [:outro_id, :user_id]
    add_index :outros_users, :user_id

    create_table :templates_users, :id => false do |t|
        t.references :template
        t.references :user
    end
    add_index :templates_users, [:template_id, :user_id]
    add_index :templates_users, :user_id
  end
end
