class AddFieldsToMessages < ActiveRecord::Migration
	def change
	  create_table :messages do |t|
	    t.integer :contact_id
	    t.integer :sold_property_id
	    t.boolean :sent, null: false,:default => false
	    t.date :sent_date_at
	    t.boolean :expired, null: false,:default => false
	    t.decimal :distance, :precision => 8, :scale => 2
	    t.string  :investment_property
	    t.string :range_property
	    t.timestamps
	  end
	end
end



