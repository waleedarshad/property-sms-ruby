class AddIndexOnUserIdAndMobileNumber < ActiveRecord::Migration
  def change
    add_index :contacts, [:user_id,:mobile_number]
  end
end
