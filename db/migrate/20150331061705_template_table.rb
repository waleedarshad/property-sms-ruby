class TemplateTable < ActiveRecord::Migration
  def change
    create_table :templates do |t|
      t.string :group
      t.text :message

      t.timestamps
    end

    change_column(:intros, :message, :text)
    change_column(:outros, :message, :text)
  end
end
