class ChangeMessageBodyDatatypeToText < ActiveRecord::Migration
  def up
      change_column :messages, :message_body, :text
  end
  def down
      # This might cause trouble if you have strings longer
      # than 255 characters.
      change_column :messages, :message_body, :string
  end
end
