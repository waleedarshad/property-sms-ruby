class CreateMessageProperties < ActiveRecord::Migration
  def change
    create_table :message_properties do |t|
      t.integer :message_id
      t.integer :sold_property_id
      t.integer :property_id
      t.decimal :distance
      t.string :investment_property
      t.string :range_property

      t.timestamps null: false
    end
  end
end
