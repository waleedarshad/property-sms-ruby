class IntroTable < ActiveRecord::Migration
  def change
    create_table :intros do |t|
      t.string :message

      t.timestamps
    end
  end
end
