class PropertyIdMessage < ActiveRecord::Migration
  def change
    add_column :messages, :property_id, :integer, :after => :contact_id
  end
end