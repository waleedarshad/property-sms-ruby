class ContactPartnersFields < ActiveRecord::Migration
  def change
  	add_column :contacts, :partner_first_name, :string, :after => :mobile_number
  	add_column :contacts, :partner_last_name, :string, :after => :partner_first_name
  	add_column :contacts, :partner_mobile_number, :string, :after => :partner_last_name
  end
end
