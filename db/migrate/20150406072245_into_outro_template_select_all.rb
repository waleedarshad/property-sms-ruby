class IntoOutroTemplateSelectAll < ActiveRecord::Migration
  def change
  	add_column :intros, :all_users, :boolean, default: false, after: :message
  	add_column :outros, :all_users, :boolean, default: false, after: :message
  	add_column :templates, :all_users, :boolean, default: false, after: :message
  end
end
