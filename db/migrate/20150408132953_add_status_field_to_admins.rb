class AddStatusFieldToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :status, :integer,:default => 0,:null=> false
  end
end
