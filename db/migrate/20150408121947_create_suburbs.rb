class CreateSuburbs < ActiveRecord::Migration
  def change
    create_table :suburbs do |t|
      t.string :state
      t.string :address
      t.integer :close_range
      t.integer :medium_range

      t.timestamps null: false
    end
  end
end
