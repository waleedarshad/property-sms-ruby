class ContactsProperties < ActiveRecord::Migration
  def change
	remove_column :contacts, :unit_number
	remove_column :contacts, :house_number
	remove_column :contacts, :street
	remove_column :contacts, :type
	remove_column :contacts, :suburb
	remove_column :contacts, :state
	remove_column :contacts, :postcode

    create_table :properties do |t|
      t.references :contact
      t.string :unit_number
      t.string :house_number
      t.string :street
      t.boolean :investment_property, default: false
      t.string :suburb
      t.string :state
      t.string :postcode
      t.timestamps null: false
    end
  end
end
