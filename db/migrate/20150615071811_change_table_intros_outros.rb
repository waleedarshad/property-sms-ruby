class ChangeTableIntrosOutros < ActiveRecord::Migration
  def change
  	change_table :intros_users do |t|
      t.boolean :admin,:default => true,null: false
    end

		change_table :outros_users do |t|
	    t.boolean :admin,:default => true,null: false
	  end
	  add_column :intros_users, :id, :primary_key
	  add_column :outros_users, :id, :primary_key
  end
end
