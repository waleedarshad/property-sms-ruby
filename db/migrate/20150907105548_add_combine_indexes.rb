class AddCombineIndexes < ActiveRecord::Migration
  def change
    add_index :messages, [:contact_id,:sent]
    add_index :contacts, [:id,:deleted]
    add_index :suburbs, [:address,:state]
    add_index :messages, [:contact_id,:sold_property_id]
  end
end
