class OutroTable < ActiveRecord::Migration
  def change
    create_table :outros do |t|
      t.string :message

      t.timestamps
    end
  end
end
